using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace CrestronModule_SHARP_LC_70LE745U_IP_V1_0_PROCESSOR
{
    public class CrestronModuleClass_SHARP_LC_70LE745U_IP_V1_0_PROCESSOR : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput RESPONSE_TIMEOUT;
        Crestron.Logos.SplusObjects.DigitalInput AUDIOMODE;
        Crestron.Logos.SplusObjects.DigitalInput SURROUNDMODE;
        Crestron.Logos.SplusObjects.DigitalInput CLOSEDCAPTION;
        Crestron.Logos.SplusObjects.DigitalInput CHANNELUP;
        Crestron.Logos.SplusObjects.DigitalInput CHANNELDOWN;
        Crestron.Logos.SplusObjects.DigitalInput AIR;
        Crestron.Logos.SplusObjects.DigitalInput CABLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> KP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> POWER;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> INPUT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> AVMODE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> ASPECT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SLEEP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> EFFECT3D;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> POLL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> IRKEYS;
        Crestron.Logos.SplusObjects.StringInput FROM_DEVICE;
        Crestron.Logos.SplusObjects.DigitalOutput IWAITINGFORRESPONSE;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_POWER;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_INPUT;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_AVMODE;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_ASPECT;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_VOLUMEMUTE;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_SLEEP;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_EFFECT3D;
        Crestron.Logos.SplusObjects.AnalogOutput CURRENT_VOLUME;
        Crestron.Logos.SplusObjects.StringOutput NUMBER__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput TO_DEVICE;
        ushort ICOMMAND = 0;
        ushort IVALUE = 0;
        ushort IVALUEIN = 0;
        ushort ALOC = 0;
        ushort IPOWERQUEUE = 0;
        ushort IINPUTQUEUE = 0;
        ushort IAVMODEQUEUE = 0;
        ushort IASPECTQUEUE = 0;
        ushort IVOLUMEMUTEQUEUE = 0;
        ushort ISLEEPQUEUE = 0;
        ushort IEFFECT3DQUEUE = 0;
        ushort IAUDIOMODEQUEUE = 0;
        ushort ISURROUNDMODEQUEUE = 0;
        ushort ICLOSEDCAPTIONQUEUE = 0;
        ushort ICHANNELREQQUEUE = 0;
        ushort ICHANNELUPQUEUE = 0;
        ushort ICHANNELDOWNQUEUE = 0;
        ushort IPOLLQUEUE = 0;
        ushort IPOWERSENT = 0;
        ushort IINPUTSENT = 0;
        ushort IAVMODESENT = 0;
        ushort IASPECTSENT = 0;
        ushort IVOLUMEMUTESENT = 0;
        ushort ISLEEPSENT = 0;
        ushort IEFFECT3DSENT = 0;
        ushort IPOLLSENT = 0;
        ushort IIRKEYSSENT = 0;
        ushort IIRKEYSQUEUE = 0;
        CrestronString STODEVICETEMP;
        CrestronString SNUMBER;
        CrestronString SCHANNELF;
        CrestronString SCHANNELR;
        CrestronString SCHANNELTEMP;
        private CrestronString SENDPOWER (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 101;
            
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 104;
                        STODEVICETEMP  .UpdateValue ( "POWR1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 106;
                        STODEVICETEMP  .UpdateValue ( "POWR0   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 108;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDINPUT (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 115;
            
                {
                int __SPLS_TMPVAR__SWTCH_2__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 118;
                        STODEVICETEMP  .UpdateValue ( "IAVD5   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 120;
                        STODEVICETEMP  .UpdateValue ( "IAVD6   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 122;
                        STODEVICETEMP  .UpdateValue ( "IAVD8   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 124;
                        STODEVICETEMP  .UpdateValue ( "IAVD1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 126;
                        STODEVICETEMP  .UpdateValue ( "IAVD2   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 128;
                        STODEVICETEMP  .UpdateValue ( "IAVD3   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 130;
                        STODEVICETEMP  .UpdateValue ( "IAVD4   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 132;
                        STODEVICETEMP  .UpdateValue ( "ITVD0   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 9) ) ) ) 
                        {
                        __context__.SourceCodeLine = 134;
                        STODEVICETEMP  .UpdateValue ( "IAVD7   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 136;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDAVMODE (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 142;
            
                {
                int __SPLS_TMPVAR__SWTCH_3__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 145;
                        STODEVICETEMP  .UpdateValue ( "AVMD1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 147;
                        STODEVICETEMP  .UpdateValue ( "AVMD2   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 149;
                        STODEVICETEMP  .UpdateValue ( "AVMD3   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 151;
                        STODEVICETEMP  .UpdateValue ( "AVMD4   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 153;
                        STODEVICETEMP  .UpdateValue ( "AVMD5   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 155;
                        STODEVICETEMP  .UpdateValue ( "AVMD6   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 157;
                        STODEVICETEMP  .UpdateValue ( "AVMD7   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 159;
                        STODEVICETEMP  .UpdateValue ( "AVMD8   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 9) ) ) ) 
                        {
                        __context__.SourceCodeLine = 161;
                        STODEVICETEMP  .UpdateValue ( "AVMD14  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 10) ) ) ) 
                        {
                        __context__.SourceCodeLine = 163;
                        STODEVICETEMP  .UpdateValue ( "AVMD15  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 11) ) ) ) 
                        {
                        __context__.SourceCodeLine = 165;
                        STODEVICETEMP  .UpdateValue ( "AVMD16  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_3__ == ( 12) ) ) ) 
                        {
                        __context__.SourceCodeLine = 167;
                        STODEVICETEMP  .UpdateValue ( "AVMD100 \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 169;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDASPECT (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 175;
            
                {
                int __SPLS_TMPVAR__SWTCH_4__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 178;
                        STODEVICETEMP  .UpdateValue ( "WIDE1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 180;
                        STODEVICETEMP  .UpdateValue ( "WIDE2   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 182;
                        STODEVICETEMP  .UpdateValue ( "WIDE3   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 184;
                        STODEVICETEMP  .UpdateValue ( "WIDE4   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 186;
                        STODEVICETEMP  .UpdateValue ( "WIDE5   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 188;
                        STODEVICETEMP  .UpdateValue ( "WIDE6   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 190;
                        STODEVICETEMP  .UpdateValue ( "WIDE7   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 192;
                        STODEVICETEMP  .UpdateValue ( "WIDE8   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 9) ) ) ) 
                        {
                        __context__.SourceCodeLine = 194;
                        STODEVICETEMP  .UpdateValue ( "WIDE9   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 10) ) ) ) 
                        {
                        __context__.SourceCodeLine = 196;
                        STODEVICETEMP  .UpdateValue ( "WIDE10  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_4__ == ( 11) ) ) ) 
                        {
                        __context__.SourceCodeLine = 198;
                        STODEVICETEMP  .UpdateValue ( "WIDE11  \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 200;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDVOLUMEMUTE (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 206;
            
                {
                int __SPLS_TMPVAR__SWTCH_5__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_5__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 209;
                        STODEVICETEMP  .UpdateValue ( "MUTE1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_5__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 211;
                        STODEVICETEMP  .UpdateValue ( "MUTE2   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 213;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDSLEEP (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 219;
            
                {
                int __SPLS_TMPVAR__SWTCH_6__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 222;
                        STODEVICETEMP  .UpdateValue ( "OFTM0   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 224;
                        STODEVICETEMP  .UpdateValue ( "OFTM1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 226;
                        STODEVICETEMP  .UpdateValue ( "OFTM2   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 228;
                        STODEVICETEMP  .UpdateValue ( "OFTM3   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_6__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 230;
                        STODEVICETEMP  .UpdateValue ( "OFTM4   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 232;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDEFFECT3D (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 238;
            
                {
                int __SPLS_TMPVAR__SWTCH_7__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 241;
                        STODEVICETEMP  .UpdateValue ( "TDCH0   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 243;
                        STODEVICETEMP  .UpdateValue ( "TDCH1   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 245;
                        STODEVICETEMP  .UpdateValue ( "TDCH2   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 247;
                        STODEVICETEMP  .UpdateValue ( "TDCH3   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 249;
                        STODEVICETEMP  .UpdateValue ( "TDCH4   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 251;
                        STODEVICETEMP  .UpdateValue ( "TDCH5   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 253;
                        STODEVICETEMP  .UpdateValue ( "TDCH6   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_7__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 255;
                        STODEVICETEMP  .UpdateValue ( "TDCH7   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 257;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDPOLL (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 263;
            
                {
                int __SPLS_TMPVAR__SWTCH_8__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 266;
                        STODEVICETEMP  .UpdateValue ( "POWR?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 268;
                        STODEVICETEMP  .UpdateValue ( "IAVD?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 270;
                        STODEVICETEMP  .UpdateValue ( "AVMD?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 272;
                        STODEVICETEMP  .UpdateValue ( "WIDE?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 274;
                        STODEVICETEMP  .UpdateValue ( "MUTE?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 276;
                        STODEVICETEMP  .UpdateValue ( "OFTM?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 278;
                        STODEVICETEMP  .UpdateValue ( "TDCH?   \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_8__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 280;
                        STODEVICETEMP  .UpdateValue ( "VOLM?   \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 282;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private CrestronString SENDIRKEYS (  SplusExecutionContext __context__, ushort IVALUE ) 
            { 
            
            __context__.SourceCodeLine = 288;
            
                {
                int __SPLS_TMPVAR__SWTCH_9__ = ((int)IVALUE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 291;
                        STODEVICETEMP  .UpdateValue ( "RCKY00  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 293;
                        STODEVICETEMP  .UpdateValue ( "RCKY01  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 295;
                        STODEVICETEMP  .UpdateValue ( "RCKY02  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 297;
                        STODEVICETEMP  .UpdateValue ( "RCKY03  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 299;
                        STODEVICETEMP  .UpdateValue ( "RCKY04  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 301;
                        STODEVICETEMP  .UpdateValue ( "RCKY05  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 303;
                        STODEVICETEMP  .UpdateValue ( "RCKY06  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 305;
                        STODEVICETEMP  .UpdateValue ( "RCKY07  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 9) ) ) ) 
                        {
                        __context__.SourceCodeLine = 307;
                        STODEVICETEMP  .UpdateValue ( "RCKY08  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 10) ) ) ) 
                        {
                        __context__.SourceCodeLine = 309;
                        STODEVICETEMP  .UpdateValue ( "RCKY09  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 11) ) ) ) 
                        {
                        __context__.SourceCodeLine = 311;
                        STODEVICETEMP  .UpdateValue ( "RCKY10  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 12) ) ) ) 
                        {
                        __context__.SourceCodeLine = 313;
                        STODEVICETEMP  .UpdateValue ( "RCKY11  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 13) ) ) ) 
                        {
                        __context__.SourceCodeLine = 315;
                        STODEVICETEMP  .UpdateValue ( "RCKY12  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 14) ) ) ) 
                        {
                        __context__.SourceCodeLine = 317;
                        STODEVICETEMP  .UpdateValue ( "RCKY13  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 15) ) ) ) 
                        {
                        __context__.SourceCodeLine = 319;
                        STODEVICETEMP  .UpdateValue ( "RCKY14  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 16) ) ) ) 
                        {
                        __context__.SourceCodeLine = 321;
                        STODEVICETEMP  .UpdateValue ( "RCKY15  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 17) ) ) ) 
                        {
                        __context__.SourceCodeLine = 323;
                        STODEVICETEMP  .UpdateValue ( "RCKY16  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 18) ) ) ) 
                        {
                        __context__.SourceCodeLine = 325;
                        STODEVICETEMP  .UpdateValue ( "RCKY17  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 19) ) ) ) 
                        {
                        __context__.SourceCodeLine = 327;
                        STODEVICETEMP  .UpdateValue ( "RCKY18  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 20) ) ) ) 
                        {
                        __context__.SourceCodeLine = 329;
                        STODEVICETEMP  .UpdateValue ( "RCKY19  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 21) ) ) ) 
                        {
                        __context__.SourceCodeLine = 331;
                        STODEVICETEMP  .UpdateValue ( "RCKY20  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 22) ) ) ) 
                        {
                        __context__.SourceCodeLine = 333;
                        STODEVICETEMP  .UpdateValue ( "RCKY21  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 23) ) ) ) 
                        {
                        __context__.SourceCodeLine = 335;
                        STODEVICETEMP  .UpdateValue ( "RCKY22  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 24) ) ) ) 
                        {
                        __context__.SourceCodeLine = 337;
                        STODEVICETEMP  .UpdateValue ( "RCKY23  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 25) ) ) ) 
                        {
                        __context__.SourceCodeLine = 339;
                        STODEVICETEMP  .UpdateValue ( "RCKY24  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 26) ) ) ) 
                        {
                        __context__.SourceCodeLine = 341;
                        STODEVICETEMP  .UpdateValue ( "RCKY27  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 27) ) ) ) 
                        {
                        __context__.SourceCodeLine = 343;
                        STODEVICETEMP  .UpdateValue ( "RCKY28  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 28) ) ) ) 
                        {
                        __context__.SourceCodeLine = 345;
                        STODEVICETEMP  .UpdateValue ( "RCKY29  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 29) ) ) ) 
                        {
                        __context__.SourceCodeLine = 347;
                        STODEVICETEMP  .UpdateValue ( "RCKY30  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 30) ) ) ) 
                        {
                        __context__.SourceCodeLine = 349;
                        STODEVICETEMP  .UpdateValue ( "RCKY34  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 31) ) ) ) 
                        {
                        __context__.SourceCodeLine = 351;
                        STODEVICETEMP  .UpdateValue ( "RCKY35  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 32) ) ) ) 
                        {
                        __context__.SourceCodeLine = 353;
                        STODEVICETEMP  .UpdateValue ( "RCKY36  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 33) ) ) ) 
                        {
                        __context__.SourceCodeLine = 355;
                        STODEVICETEMP  .UpdateValue ( "RCKY38  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 34) ) ) ) 
                        {
                        __context__.SourceCodeLine = 357;
                        STODEVICETEMP  .UpdateValue ( "RCKY39  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 35) ) ) ) 
                        {
                        __context__.SourceCodeLine = 359;
                        STODEVICETEMP  .UpdateValue ( "RCKY40  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 36) ) ) ) 
                        {
                        __context__.SourceCodeLine = 361;
                        STODEVICETEMP  .UpdateValue ( "RCKY41  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 37) ) ) ) 
                        {
                        __context__.SourceCodeLine = 363;
                        STODEVICETEMP  .UpdateValue ( "RCKY42  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 38) ) ) ) 
                        {
                        __context__.SourceCodeLine = 365;
                        STODEVICETEMP  .UpdateValue ( "RCKY43  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 39) ) ) ) 
                        {
                        __context__.SourceCodeLine = 367;
                        STODEVICETEMP  .UpdateValue ( "RCKY44  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 40) ) ) ) 
                        {
                        __context__.SourceCodeLine = 369;
                        STODEVICETEMP  .UpdateValue ( "RCKY45  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 41) ) ) ) 
                        {
                        __context__.SourceCodeLine = 371;
                        STODEVICETEMP  .UpdateValue ( "RCKY46  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 42) ) ) ) 
                        {
                        __context__.SourceCodeLine = 373;
                        STODEVICETEMP  .UpdateValue ( "RCKY47  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 43) ) ) ) 
                        {
                        __context__.SourceCodeLine = 375;
                        STODEVICETEMP  .UpdateValue ( "RCKY48  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 44) ) ) ) 
                        {
                        __context__.SourceCodeLine = 377;
                        STODEVICETEMP  .UpdateValue ( "RCKY49  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 45) ) ) ) 
                        {
                        __context__.SourceCodeLine = 379;
                        STODEVICETEMP  .UpdateValue ( "RCKY50  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 46) ) ) ) 
                        {
                        __context__.SourceCodeLine = 381;
                        STODEVICETEMP  .UpdateValue ( "RCKY51  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 47) ) ) ) 
                        {
                        __context__.SourceCodeLine = 383;
                        STODEVICETEMP  .UpdateValue ( "RCKY52  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 48) ) ) ) 
                        {
                        __context__.SourceCodeLine = 385;
                        STODEVICETEMP  .UpdateValue ( "RCKY53  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 49) ) ) ) 
                        {
                        __context__.SourceCodeLine = 387;
                        STODEVICETEMP  .UpdateValue ( "RCKY54  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 50) ) ) ) 
                        {
                        __context__.SourceCodeLine = 389;
                        STODEVICETEMP  .UpdateValue ( "RCKY55  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 51) ) ) ) 
                        {
                        __context__.SourceCodeLine = 391;
                        STODEVICETEMP  .UpdateValue ( "RCKY56  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 52) ) ) ) 
                        {
                        __context__.SourceCodeLine = 393;
                        STODEVICETEMP  .UpdateValue ( "RCKY57  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 53) ) ) ) 
                        {
                        __context__.SourceCodeLine = 395;
                        STODEVICETEMP  .UpdateValue ( "RCKY58  \u000D"  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_9__ == ( 54) ) ) ) 
                        {
                        __context__.SourceCodeLine = 397;
                        STODEVICETEMP  .UpdateValue ( "RCKY59  \u000D"  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            __context__.SourceCodeLine = 399;
            return ( STODEVICETEMP ) ; 
            
            }
            
        private void SENDCOMMANDQUEUED (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 404;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IPOWERQUEUE != 0) ) || Functions.TestForTrue ( Functions.BoolToInt (IINPUTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAVMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IASPECTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISLEEPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IEFFECT3DQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELREQQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELUPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0) )) ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 408;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOWERQUEUE != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 410;
                    TO_DEVICE  .UpdateValue ( SENDPOWER (  __context__ , (ushort)( IPOWERQUEUE ))  ) ; 
                    __context__.SourceCodeLine = 411;
                    ICOMMAND = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 412;
                    IPOWERSENT = (ushort) ( IPOWERQUEUE ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 414;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IINPUTQUEUE != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 416;
                        TO_DEVICE  .UpdateValue ( SENDINPUT (  __context__ , (ushort)( IINPUTQUEUE ))  ) ; 
                        __context__.SourceCodeLine = 417;
                        ICOMMAND = (ushort) ( 2 ) ; 
                        __context__.SourceCodeLine = 418;
                        IINPUTSENT = (ushort) ( IINPUTQUEUE ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 420;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAVMODEQUEUE != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 422;
                            TO_DEVICE  .UpdateValue ( SENDAVMODE (  __context__ , (ushort)( IAVMODEQUEUE ))  ) ; 
                            __context__.SourceCodeLine = 423;
                            ICOMMAND = (ushort) ( 3 ) ; 
                            __context__.SourceCodeLine = 424;
                            IAVMODESENT = (ushort) ( IAVMODEQUEUE ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 426;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IASPECTQUEUE != 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 428;
                                TO_DEVICE  .UpdateValue ( SENDASPECT (  __context__ , (ushort)( IASPECTQUEUE ))  ) ; 
                                __context__.SourceCodeLine = 429;
                                ICOMMAND = (ushort) ( 4 ) ; 
                                __context__.SourceCodeLine = 430;
                                IASPECTSENT = (ushort) ( IASPECTQUEUE ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 432;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 434;
                                    TO_DEVICE  .UpdateValue ( SENDVOLUMEMUTE (  __context__ , (ushort)( IVOLUMEMUTEQUEUE ))  ) ; 
                                    __context__.SourceCodeLine = 435;
                                    ICOMMAND = (ushort) ( 5 ) ; 
                                    __context__.SourceCodeLine = 436;
                                    IVOLUMEMUTESENT = (ushort) ( IVOLUMEMUTEQUEUE ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 438;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISLEEPQUEUE != 0))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 440;
                                        TO_DEVICE  .UpdateValue ( SENDSLEEP (  __context__ , (ushort)( ISLEEPQUEUE ))  ) ; 
                                        __context__.SourceCodeLine = 441;
                                        ICOMMAND = (ushort) ( 6 ) ; 
                                        __context__.SourceCodeLine = 442;
                                        ISLEEPSENT = (ushort) ( ISLEEPQUEUE ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 444;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IEFFECT3DQUEUE != 0))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 446;
                                            TO_DEVICE  .UpdateValue ( SENDEFFECT3D (  __context__ , (ushort)( IEFFECT3DQUEUE ))  ) ; 
                                            __context__.SourceCodeLine = 447;
                                            ICOMMAND = (ushort) ( 7 ) ; 
                                            __context__.SourceCodeLine = 448;
                                            IEFFECT3DSENT = (ushort) ( IEFFECT3DQUEUE ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 450;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 452;
                                                TO_DEVICE  .UpdateValue ( "ACHA1   \u000D"  ) ; 
                                                __context__.SourceCodeLine = 453;
                                                ICOMMAND = (ushort) ( 9 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 455;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 457;
                                                    TO_DEVICE  .UpdateValue ( "ACSU0   \u000D"  ) ; 
                                                    __context__.SourceCodeLine = 458;
                                                    ICOMMAND = (ushort) ( 10 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 460;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 462;
                                                        TO_DEVICE  .UpdateValue ( "CLCP1   \u000D"  ) ; 
                                                        __context__.SourceCodeLine = 463;
                                                        ICOMMAND = (ushort) ( 11 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 465;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELREQQUEUE != 0))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 467;
                                                            TO_DEVICE  .UpdateValue ( SCHANNELTEMP  ) ; 
                                                            __context__.SourceCodeLine = 468;
                                                            ICOMMAND = (ushort) ( 12 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 470;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELUPQUEUE != 0))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 472;
                                                                TO_DEVICE  .UpdateValue ( "CHUP1   \u000D"  ) ; 
                                                                __context__.SourceCodeLine = 473;
                                                                ICOMMAND = (ushort) ( 13 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 475;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 477;
                                                                    TO_DEVICE  .UpdateValue ( "CHDW1   \u000D"  ) ; 
                                                                    __context__.SourceCodeLine = 478;
                                                                    ICOMMAND = (ushort) ( 14 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 480;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IIRKEYSQUEUE != 0))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 482;
                                                                        TO_DEVICE  .UpdateValue ( SENDIRKEYS (  __context__ , (ushort)( IIRKEYSQUEUE ))  ) ; 
                                                                        __context__.SourceCodeLine = 483;
                                                                        ICOMMAND = (ushort) ( 15 ) ; 
                                                                        __context__.SourceCodeLine = 484;
                                                                        IIRKEYSSENT = (ushort) ( IIRKEYSQUEUE ) ; 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            
            }
            
        object POWER_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 496;
                IPOWERQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 497;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 499;
                    TO_DEVICE  .UpdateValue ( SENDPOWER (  __context__ , (ushort)( IPOWERQUEUE ))  ) ; 
                    __context__.SourceCodeLine = 500;
                    ICOMMAND = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 501;
                    IPOWERSENT = (ushort) ( IPOWERQUEUE ) ; 
                    __context__.SourceCodeLine = 502;
                    IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object INPUT_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 508;
            IINPUTQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 509;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 511;
                TO_DEVICE  .UpdateValue ( SENDINPUT (  __context__ , (ushort)( IINPUTQUEUE ))  ) ; 
                __context__.SourceCodeLine = 512;
                ICOMMAND = (ushort) ( 2 ) ; 
                __context__.SourceCodeLine = 513;
                IINPUTSENT = (ushort) ( IINPUTQUEUE ) ; 
                __context__.SourceCodeLine = 514;
                IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object AVMODE_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 520;
        IAVMODEQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 521;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 523;
            TO_DEVICE  .UpdateValue ( SENDAVMODE (  __context__ , (ushort)( IAVMODEQUEUE ))  ) ; 
            __context__.SourceCodeLine = 524;
            ICOMMAND = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 525;
            IAVMODESENT = (ushort) ( IAVMODEQUEUE ) ; 
            __context__.SourceCodeLine = 526;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ASPECT_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 532;
        IASPECTQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 533;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 535;
            TO_DEVICE  .UpdateValue ( SENDASPECT (  __context__ , (ushort)( IASPECTQUEUE ))  ) ; 
            __context__.SourceCodeLine = 536;
            ICOMMAND = (ushort) ( 4 ) ; 
            __context__.SourceCodeLine = 537;
            IASPECTSENT = (ushort) ( IASPECTQUEUE ) ; 
            __context__.SourceCodeLine = 538;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTE_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 544;
        IVOLUMEMUTEQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 545;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 547;
            TO_DEVICE  .UpdateValue ( SENDVOLUMEMUTE (  __context__ , (ushort)( IVOLUMEMUTEQUEUE ))  ) ; 
            __context__.SourceCodeLine = 548;
            ICOMMAND = (ushort) ( 5 ) ; 
            __context__.SourceCodeLine = 549;
            IVOLUMEMUTESENT = (ushort) ( IVOLUMEMUTEQUEUE ) ; 
            __context__.SourceCodeLine = 550;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SLEEP_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 556;
        ISLEEPQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 557;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 559;
            TO_DEVICE  .UpdateValue ( SENDSLEEP (  __context__ , (ushort)( ISLEEPQUEUE ))  ) ; 
            __context__.SourceCodeLine = 560;
            ICOMMAND = (ushort) ( 6 ) ; 
            __context__.SourceCodeLine = 561;
            ISLEEPSENT = (ushort) ( ISLEEPQUEUE ) ; 
            __context__.SourceCodeLine = 562;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object EFFECT3D_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 568;
        IEFFECT3DQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 569;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 571;
            TO_DEVICE  .UpdateValue ( SENDEFFECT3D (  __context__ , (ushort)( IEFFECT3DQUEUE ))  ) ; 
            __context__.SourceCodeLine = 572;
            ICOMMAND = (ushort) ( 7 ) ; 
            __context__.SourceCodeLine = 573;
            IEFFECT3DSENT = (ushort) ( IEFFECT3DQUEUE ) ; 
            __context__.SourceCodeLine = 574;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 580;
        IPOLLQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 581;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 583;
            TO_DEVICE  .UpdateValue ( SENDPOLL (  __context__ , (ushort)( IPOLLQUEUE ))  ) ; 
            __context__.SourceCodeLine = 584;
            ICOMMAND = (ushort) ( 30 ) ; 
            __context__.SourceCodeLine = 585;
            IPOLLSENT = (ushort) ( IPOLLQUEUE ) ; 
            __context__.SourceCodeLine = 586;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object AUDIOMODE_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 592;
        IAUDIOMODEQUEUE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 593;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 595;
            TO_DEVICE  .UpdateValue ( "ACHA1   \u000D"  ) ; 
            __context__.SourceCodeLine = 596;
            ICOMMAND = (ushort) ( 9 ) ; 
            __context__.SourceCodeLine = 597;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SURROUNDMODE_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 604;
        ISURROUNDMODEQUEUE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 605;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 607;
            TO_DEVICE  .UpdateValue ( "ACSU0   \u000D"  ) ; 
            __context__.SourceCodeLine = 608;
            ICOMMAND = (ushort) ( 10 ) ; 
            __context__.SourceCodeLine = 609;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CLOSEDCAPTION_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 615;
        ICLOSEDCAPTIONQUEUE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 616;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 618;
            TO_DEVICE  .UpdateValue ( "CLCP1   \u000D"  ) ; 
            __context__.SourceCodeLine = 619;
            ICOMMAND = (ushort) ( 11 ) ; 
            __context__.SourceCodeLine = 620;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHANNELUP_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 626;
        ICHANNELUPQUEUE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 627;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 629;
            TO_DEVICE  .UpdateValue ( "CHUP1   \u000D"  ) ; 
            __context__.SourceCodeLine = 630;
            ICOMMAND = (ushort) ( 13 ) ; 
            __context__.SourceCodeLine = 631;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CHANNELDOWN_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 637;
        ICHANNELDOWNQUEUE = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 638;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 640;
            TO_DEVICE  .UpdateValue ( "CHDW1   \u000D"  ) ; 
            __context__.SourceCodeLine = 641;
            ICOMMAND = (ushort) ( 14 ) ; 
            __context__.SourceCodeLine = 642;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FROM_DEVICE_OnChange_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 648;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FROM_DEVICE == "OK\u000D"))  ) ) 
            { 
            __context__.SourceCodeLine = 650;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 652;
                CURRENT_POWER  .Value = (ushort) ( IPOWERSENT ) ; 
                __context__.SourceCodeLine = 653;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_POWER  .Value == IPOWERQUEUE))  ) ) 
                    {
                    __context__.SourceCodeLine = 654;
                    IPOWERQUEUE = (ushort) ( 0 ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 656;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 2))  ) ) 
                    { 
                    __context__.SourceCodeLine = 658;
                    CURRENT_INPUT  .Value = (ushort) ( IINPUTSENT ) ; 
                    __context__.SourceCodeLine = 659;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_INPUT  .Value == IINPUTQUEUE))  ) ) 
                        {
                        __context__.SourceCodeLine = 660;
                        IINPUTQUEUE = (ushort) ( 0 ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 662;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 3))  ) ) 
                        { 
                        __context__.SourceCodeLine = 664;
                        CURRENT_AVMODE  .Value = (ushort) ( IAVMODESENT ) ; 
                        __context__.SourceCodeLine = 665;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_AVMODE  .Value == IAVMODEQUEUE))  ) ) 
                            {
                            __context__.SourceCodeLine = 666;
                            IAVMODEQUEUE = (ushort) ( 0 ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 668;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 4))  ) ) 
                            { 
                            __context__.SourceCodeLine = 670;
                            CURRENT_ASPECT  .Value = (ushort) ( IASPECTSENT ) ; 
                            __context__.SourceCodeLine = 671;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_ASPECT  .Value == IASPECTQUEUE))  ) ) 
                                {
                                __context__.SourceCodeLine = 672;
                                IASPECTQUEUE = (ushort) ( 0 ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 674;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 5))  ) ) 
                                { 
                                __context__.SourceCodeLine = 676;
                                CURRENT_VOLUMEMUTE  .Value = (ushort) ( IVOLUMEMUTESENT ) ; 
                                __context__.SourceCodeLine = 677;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_VOLUMEMUTE  .Value == IVOLUMEMUTEQUEUE))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 678;
                                    IVOLUMEMUTEQUEUE = (ushort) ( 0 ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 680;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 6))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 682;
                                    CURRENT_SLEEP  .Value = (ushort) ( ISLEEPSENT ) ; 
                                    __context__.SourceCodeLine = 683;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_SLEEP  .Value == ISLEEPQUEUE))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 684;
                                        ISLEEPQUEUE = (ushort) ( 0 ) ; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 686;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 7))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 688;
                                        CURRENT_EFFECT3D  .Value = (ushort) ( IEFFECT3DSENT ) ; 
                                        __context__.SourceCodeLine = 689;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CURRENT_EFFECT3D  .Value == IEFFECT3DQUEUE))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 690;
                                            IEFFECT3DQUEUE = (ushort) ( 0 ) ; 
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 692;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 9))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 693;
                                            IAUDIOMODEQUEUE = (ushort) ( 0 ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 694;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 10))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 695;
                                                ISURROUNDMODEQUEUE = (ushort) ( 0 ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 696;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 11))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 697;
                                                    ICLOSEDCAPTIONQUEUE = (ushort) ( 0 ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 698;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 12))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 699;
                                                        ICHANNELREQQUEUE = (ushort) ( 0 ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 700;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 13))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 701;
                                                            ICHANNELUPQUEUE = (ushort) ( 0 ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 702;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 14))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 703;
                                                                ICHANNELDOWNQUEUE = (ushort) ( 0 ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 704;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 15))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 705;
                                                                    IIRKEYSQUEUE = (ushort) ( 0 ) ; 
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 707;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (FROM_DEVICE == "ERR\u000D") ) || Functions.TestForTrue ( Functions.BoolToInt (FROM_DEVICE == "\u00FF\u00FF\u00FF") )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 709;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 710;
                    IPOWERQUEUE = (ushort) ( 0 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 711;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 2))  ) ) 
                        {
                        __context__.SourceCodeLine = 712;
                        IINPUTQUEUE = (ushort) ( 0 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 713;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 3))  ) ) 
                            {
                            __context__.SourceCodeLine = 714;
                            IAVMODEQUEUE = (ushort) ( 0 ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 715;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 4))  ) ) 
                                {
                                __context__.SourceCodeLine = 716;
                                IASPECTQUEUE = (ushort) ( 0 ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 717;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 5))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 718;
                                    IVOLUMEMUTEQUEUE = (ushort) ( 0 ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 719;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 6))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 720;
                                        ISLEEPQUEUE = (ushort) ( 0 ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 721;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 7))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 722;
                                            IEFFECT3DQUEUE = (ushort) ( 0 ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 723;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 9))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 724;
                                                IAUDIOMODEQUEUE = (ushort) ( 0 ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 725;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 10))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 726;
                                                    ISURROUNDMODEQUEUE = (ushort) ( 0 ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 727;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 11))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 728;
                                                        ICLOSEDCAPTIONQUEUE = (ushort) ( 0 ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 729;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 12))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 730;
                                                            ICHANNELREQQUEUE = (ushort) ( 0 ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 731;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 13))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 732;
                                                                ICHANNELUPQUEUE = (ushort) ( 0 ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 733;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 14))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 734;
                                                                    ICHANNELDOWNQUEUE = (ushort) ( 0 ) ; 
                                                                    }
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 735;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 15))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 736;
                                                                        IIRKEYSQUEUE = (ushort) ( 0 ) ; 
                                                                        }
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 737;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOMMAND == 30))  ) ) 
                                                                            {
                                                                            __context__.SourceCodeLine = 738;
                                                                            IPOLLQUEUE = (ushort) ( 0 ) ; 
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 739;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 2))  ) ) 
                    {
                    __context__.SourceCodeLine = 740;
                    CURRENT_INPUT  .Value = (ushort) ( 9 ) ; 
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 744;
                IVALUEIN = (ushort) ( Functions.Atoi( FROM_DEVICE ) ) ; 
                __context__.SourceCodeLine = 745;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 747;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_10__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_10__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 750;
                                CURRENT_POWER  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_10__ == ( 0) ) ) ) 
                                {
                                __context__.SourceCodeLine = 752;
                                CURRENT_POWER  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 755;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 2))  ) ) 
                    { 
                    __context__.SourceCodeLine = 757;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_11__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 760;
                                CURRENT_INPUT  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 762;
                                CURRENT_INPUT  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 764;
                                CURRENT_INPUT  .Value = (ushort) ( 3 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 4) ) ) ) 
                                {
                                __context__.SourceCodeLine = 766;
                                CURRENT_INPUT  .Value = (ushort) ( 4 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 5) ) ) ) 
                                {
                                __context__.SourceCodeLine = 768;
                                CURRENT_INPUT  .Value = (ushort) ( 5 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 6) ) ) ) 
                                {
                                __context__.SourceCodeLine = 770;
                                CURRENT_INPUT  .Value = (ushort) ( 6 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 7) ) ) ) 
                                {
                                __context__.SourceCodeLine = 772;
                                CURRENT_INPUT  .Value = (ushort) ( 7 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_11__ == ( 8) ) ) ) 
                                {
                                __context__.SourceCodeLine = 774;
                                CURRENT_INPUT  .Value = (ushort) ( 8 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 777;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 779;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_12__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 782;
                                CURRENT_AVMODE  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 784;
                                CURRENT_AVMODE  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 786;
                                CURRENT_AVMODE  .Value = (ushort) ( 3 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 4) ) ) ) 
                                {
                                __context__.SourceCodeLine = 788;
                                CURRENT_AVMODE  .Value = (ushort) ( 4 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 5) ) ) ) 
                                {
                                __context__.SourceCodeLine = 790;
                                CURRENT_AVMODE  .Value = (ushort) ( 5 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 6) ) ) ) 
                                {
                                __context__.SourceCodeLine = 792;
                                CURRENT_AVMODE  .Value = (ushort) ( 6 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 7) ) ) ) 
                                {
                                __context__.SourceCodeLine = 794;
                                CURRENT_AVMODE  .Value = (ushort) ( 7 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 8) ) ) ) 
                                {
                                __context__.SourceCodeLine = 796;
                                CURRENT_AVMODE  .Value = (ushort) ( 8 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 14) ) ) ) 
                                {
                                __context__.SourceCodeLine = 798;
                                CURRENT_AVMODE  .Value = (ushort) ( 9 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 15) ) ) ) 
                                {
                                __context__.SourceCodeLine = 800;
                                CURRENT_AVMODE  .Value = (ushort) ( 10 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 16) ) ) ) 
                                {
                                __context__.SourceCodeLine = 802;
                                CURRENT_AVMODE  .Value = (ushort) ( 11 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_12__ == ( 100) ) ) ) 
                                {
                                __context__.SourceCodeLine = 804;
                                CURRENT_AVMODE  .Value = (ushort) ( 12 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 807;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 4))  ) ) 
                    { 
                    __context__.SourceCodeLine = 809;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_13__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 812;
                                CURRENT_ASPECT  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 814;
                                CURRENT_ASPECT  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 816;
                                CURRENT_ASPECT  .Value = (ushort) ( 3 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 4) ) ) ) 
                                {
                                __context__.SourceCodeLine = 818;
                                CURRENT_ASPECT  .Value = (ushort) ( 4 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 5) ) ) ) 
                                {
                                __context__.SourceCodeLine = 820;
                                CURRENT_ASPECT  .Value = (ushort) ( 5 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 6) ) ) ) 
                                {
                                __context__.SourceCodeLine = 822;
                                CURRENT_ASPECT  .Value = (ushort) ( 6 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 7) ) ) ) 
                                {
                                __context__.SourceCodeLine = 824;
                                CURRENT_ASPECT  .Value = (ushort) ( 7 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 8) ) ) ) 
                                {
                                __context__.SourceCodeLine = 826;
                                CURRENT_ASPECT  .Value = (ushort) ( 8 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_13__ == ( 9) ) ) ) 
                                {
                                __context__.SourceCodeLine = 828;
                                CURRENT_ASPECT  .Value = (ushort) ( 9 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 831;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 5))  ) ) 
                    { 
                    __context__.SourceCodeLine = 833;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_14__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_14__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 836;
                                CURRENT_VOLUMEMUTE  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_14__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 838;
                                CURRENT_VOLUMEMUTE  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 841;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 6))  ) ) 
                    { 
                    __context__.SourceCodeLine = 843;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_15__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_15__ == ( 0) ) ) ) 
                                {
                                __context__.SourceCodeLine = 846;
                                CURRENT_SLEEP  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_15__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 848;
                                CURRENT_SLEEP  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_15__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 850;
                                CURRENT_SLEEP  .Value = (ushort) ( 3 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_15__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 852;
                                CURRENT_SLEEP  .Value = (ushort) ( 4 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_15__ == ( 4) ) ) ) 
                                {
                                __context__.SourceCodeLine = 854;
                                CURRENT_SLEEP  .Value = (ushort) ( 5 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 857;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 7))  ) ) 
                    { 
                    __context__.SourceCodeLine = 859;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_16__ = ((int)IVALUEIN);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 862;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 1 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 864;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 2 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 866;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 3 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 4) ) ) ) 
                                {
                                __context__.SourceCodeLine = 868;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 4 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 5) ) ) ) 
                                {
                                __context__.SourceCodeLine = 870;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 5 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 6) ) ) ) 
                                {
                                __context__.SourceCodeLine = 872;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 6 ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_16__ == ( 7) ) ) ) 
                                {
                                __context__.SourceCodeLine = 874;
                                CURRENT_EFFECT3D  .Value = (ushort) ( 7 ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 877;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOLLSENT == 8))  ) ) 
                    { 
                    __context__.SourceCodeLine = 879;
                    CURRENT_VOLUME  .Value = (ushort) ( IVALUEIN ) ; 
                    } 
                
                } 
            
            }
        
        __context__.SourceCodeLine = 882;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IPOWERQUEUE != 0) ) || Functions.TestForTrue ( Functions.BoolToInt (IINPUTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAVMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IASPECTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISLEEPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IEFFECT3DQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELREQQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELUPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IIRKEYSQUEUE != 0) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 886;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOWERQUEUE != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 888;
                TO_DEVICE  .UpdateValue ( SENDPOWER (  __context__ , (ushort)( IPOWERQUEUE ))  ) ; 
                __context__.SourceCodeLine = 889;
                ICOMMAND = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 890;
                IPOWERSENT = (ushort) ( IPOWERQUEUE ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 892;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IINPUTQUEUE != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 894;
                    TO_DEVICE  .UpdateValue ( SENDINPUT (  __context__ , (ushort)( IINPUTQUEUE ))  ) ; 
                    __context__.SourceCodeLine = 895;
                    ICOMMAND = (ushort) ( 2 ) ; 
                    __context__.SourceCodeLine = 896;
                    IINPUTSENT = (ushort) ( IINPUTQUEUE ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 898;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAVMODEQUEUE != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 900;
                        TO_DEVICE  .UpdateValue ( SENDAVMODE (  __context__ , (ushort)( IAVMODEQUEUE ))  ) ; 
                        __context__.SourceCodeLine = 901;
                        ICOMMAND = (ushort) ( 3 ) ; 
                        __context__.SourceCodeLine = 902;
                        IAVMODESENT = (ushort) ( IAVMODEQUEUE ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 904;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IASPECTQUEUE != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 906;
                            TO_DEVICE  .UpdateValue ( SENDASPECT (  __context__ , (ushort)( IASPECTQUEUE ))  ) ; 
                            __context__.SourceCodeLine = 907;
                            ICOMMAND = (ushort) ( 4 ) ; 
                            __context__.SourceCodeLine = 908;
                            IASPECTSENT = (ushort) ( IASPECTQUEUE ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 910;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 912;
                                TO_DEVICE  .UpdateValue ( SENDVOLUMEMUTE (  __context__ , (ushort)( IVOLUMEMUTEQUEUE ))  ) ; 
                                __context__.SourceCodeLine = 913;
                                ICOMMAND = (ushort) ( 5 ) ; 
                                __context__.SourceCodeLine = 914;
                                IVOLUMEMUTESENT = (ushort) ( IVOLUMEMUTEQUEUE ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 916;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISLEEPQUEUE != 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 918;
                                    TO_DEVICE  .UpdateValue ( SENDSLEEP (  __context__ , (ushort)( ISLEEPQUEUE ))  ) ; 
                                    __context__.SourceCodeLine = 919;
                                    ICOMMAND = (ushort) ( 6 ) ; 
                                    __context__.SourceCodeLine = 920;
                                    ISLEEPSENT = (ushort) ( ISLEEPQUEUE ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 922;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IEFFECT3DQUEUE != 0))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 924;
                                        TO_DEVICE  .UpdateValue ( SENDEFFECT3D (  __context__ , (ushort)( IEFFECT3DQUEUE ))  ) ; 
                                        __context__.SourceCodeLine = 925;
                                        ICOMMAND = (ushort) ( 7 ) ; 
                                        __context__.SourceCodeLine = 926;
                                        IEFFECT3DSENT = (ushort) ( IEFFECT3DQUEUE ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 928;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 930;
                                            TO_DEVICE  .UpdateValue ( "ACHA1   \u000D"  ) ; 
                                            __context__.SourceCodeLine = 931;
                                            ICOMMAND = (ushort) ( 9 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 933;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 935;
                                                TO_DEVICE  .UpdateValue ( "ACSU0   \u000D"  ) ; 
                                                __context__.SourceCodeLine = 936;
                                                ICOMMAND = (ushort) ( 10 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 938;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 940;
                                                    TO_DEVICE  .UpdateValue ( "CLCP1   \u000D"  ) ; 
                                                    __context__.SourceCodeLine = 941;
                                                    ICOMMAND = (ushort) ( 11 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 943;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELREQQUEUE != 0))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 945;
                                                        TO_DEVICE  .UpdateValue ( SCHANNELTEMP  ) ; 
                                                        __context__.SourceCodeLine = 946;
                                                        ICOMMAND = (ushort) ( 12 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 948;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELUPQUEUE != 0))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 950;
                                                            TO_DEVICE  .UpdateValue ( "CHUP1   \u000D"  ) ; 
                                                            __context__.SourceCodeLine = 951;
                                                            ICOMMAND = (ushort) ( 13 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 953;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 955;
                                                                TO_DEVICE  .UpdateValue ( "CHDW1   \u000D"  ) ; 
                                                                __context__.SourceCodeLine = 956;
                                                                ICOMMAND = (ushort) ( 14 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 958;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IIRKEYSQUEUE != 0))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 960;
                                                                    TO_DEVICE  .UpdateValue ( SENDIRKEYS (  __context__ , (ushort)( IIRKEYSQUEUE ))  ) ; 
                                                                    __context__.SourceCodeLine = 961;
                                                                    ICOMMAND = (ushort) ( 15 ) ; 
                                                                    __context__.SourceCodeLine = 962;
                                                                    IIRKEYSSENT = (ushort) ( IIRKEYSQUEUE ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 966;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 967;
        IPOLLSENT = (ushort) ( 31 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RESPONSE_TIMEOUT_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 972;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (IPOWERQUEUE != 0) ) || Functions.TestForTrue ( Functions.BoolToInt (IINPUTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAVMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IASPECTQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISLEEPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IEFFECT3DQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELREQQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELUPQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 976;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IPOWERQUEUE != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 978;
                TO_DEVICE  .UpdateValue ( SENDPOWER (  __context__ , (ushort)( IPOWERQUEUE ))  ) ; 
                __context__.SourceCodeLine = 979;
                ICOMMAND = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 980;
                IPOWERSENT = (ushort) ( IPOWERQUEUE ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 982;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IINPUTQUEUE != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 984;
                    TO_DEVICE  .UpdateValue ( SENDINPUT (  __context__ , (ushort)( IINPUTQUEUE ))  ) ; 
                    __context__.SourceCodeLine = 985;
                    ICOMMAND = (ushort) ( 2 ) ; 
                    __context__.SourceCodeLine = 986;
                    IINPUTSENT = (ushort) ( IINPUTQUEUE ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 988;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAVMODEQUEUE != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 990;
                        TO_DEVICE  .UpdateValue ( SENDAVMODE (  __context__ , (ushort)( IAVMODEQUEUE ))  ) ; 
                        __context__.SourceCodeLine = 991;
                        ICOMMAND = (ushort) ( 3 ) ; 
                        __context__.SourceCodeLine = 992;
                        IAVMODESENT = (ushort) ( IAVMODEQUEUE ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 994;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IASPECTQUEUE != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 996;
                            TO_DEVICE  .UpdateValue ( SENDASPECT (  __context__ , (ushort)( IASPECTQUEUE ))  ) ; 
                            __context__.SourceCodeLine = 997;
                            ICOMMAND = (ushort) ( 4 ) ; 
                            __context__.SourceCodeLine = 998;
                            IASPECTSENT = (ushort) ( IASPECTQUEUE ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1000;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IVOLUMEMUTEQUEUE != 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1002;
                                TO_DEVICE  .UpdateValue ( SENDVOLUMEMUTE (  __context__ , (ushort)( IVOLUMEMUTEQUEUE ))  ) ; 
                                __context__.SourceCodeLine = 1003;
                                ICOMMAND = (ushort) ( 5 ) ; 
                                __context__.SourceCodeLine = 1004;
                                IVOLUMEMUTESENT = (ushort) ( IVOLUMEMUTEQUEUE ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1006;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISLEEPQUEUE != 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1008;
                                    TO_DEVICE  .UpdateValue ( SENDSLEEP (  __context__ , (ushort)( ISLEEPQUEUE ))  ) ; 
                                    __context__.SourceCodeLine = 1009;
                                    ICOMMAND = (ushort) ( 6 ) ; 
                                    __context__.SourceCodeLine = 1010;
                                    ISLEEPSENT = (ushort) ( ISLEEPQUEUE ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1012;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IEFFECT3DQUEUE != 0))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1014;
                                        TO_DEVICE  .UpdateValue ( SENDEFFECT3D (  __context__ , (ushort)( IEFFECT3DQUEUE ))  ) ; 
                                        __context__.SourceCodeLine = 1015;
                                        ICOMMAND = (ushort) ( 7 ) ; 
                                        __context__.SourceCodeLine = 1016;
                                        IEFFECT3DSENT = (ushort) ( IEFFECT3DQUEUE ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1018;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IAUDIOMODEQUEUE != 0))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1020;
                                            TO_DEVICE  .UpdateValue ( "ACHA1   \u000D"  ) ; 
                                            __context__.SourceCodeLine = 1021;
                                            ICOMMAND = (ushort) ( 9 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1023;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ISURROUNDMODEQUEUE != 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1025;
                                                TO_DEVICE  .UpdateValue ( "ACSU0   \u000D"  ) ; 
                                                __context__.SourceCodeLine = 1026;
                                                ICOMMAND = (ushort) ( 10 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1028;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICLOSEDCAPTIONQUEUE != 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1030;
                                                    TO_DEVICE  .UpdateValue ( "CLCP1   \u000D"  ) ; 
                                                    __context__.SourceCodeLine = 1031;
                                                    ICOMMAND = (ushort) ( 11 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1033;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELREQQUEUE != 0))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1035;
                                                        TO_DEVICE  .UpdateValue ( SCHANNELTEMP  ) ; 
                                                        __context__.SourceCodeLine = 1036;
                                                        ICOMMAND = (ushort) ( 12 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1038;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELUPQUEUE != 0))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1040;
                                                            TO_DEVICE  .UpdateValue ( "CHUP1   \u000D"  ) ; 
                                                            __context__.SourceCodeLine = 1041;
                                                            ICOMMAND = (ushort) ( 13 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1043;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICHANNELDOWNQUEUE != 0))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1045;
                                                                TO_DEVICE  .UpdateValue ( "CHDW1   \u000D"  ) ; 
                                                                __context__.SourceCodeLine = 1046;
                                                                ICOMMAND = (ushort) ( 14 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1048;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IIRKEYSQUEUE != 0))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1050;
                                                                    TO_DEVICE  .UpdateValue ( SENDIRKEYS (  __context__ , (ushort)( IIRKEYSQUEUE ))  ) ; 
                                                                    __context__.SourceCodeLine = 1051;
                                                                    ICOMMAND = (ushort) ( 15 ) ; 
                                                                    __context__.SourceCodeLine = 1052;
                                                                    IIRKEYSSENT = (ushort) ( IIRKEYSQUEUE ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1056;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 1057;
        IPOLLSENT = (ushort) ( 31 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KP_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1062;
        
            {
            int __SPLS_TMPVAR__SWTCH_17__ = ((int)Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ));
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 1) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1065;
                    MakeString ( SNUMBER , "{0}0", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 2) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1067;
                    MakeString ( SNUMBER , "{0}1", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 3) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1069;
                    MakeString ( SNUMBER , "{0}2", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 4) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1071;
                    MakeString ( SNUMBER , "{0}3", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 5) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1073;
                    MakeString ( SNUMBER , "{0}4", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 6) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1075;
                    MakeString ( SNUMBER , "{0}5", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 7) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1077;
                    MakeString ( SNUMBER , "{0}6", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 8) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1079;
                    MakeString ( SNUMBER , "{0}7", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 9) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1081;
                    MakeString ( SNUMBER , "{0}8", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 10) ) ) ) 
                    {
                    __context__.SourceCodeLine = 1083;
                    MakeString ( SNUMBER , "{0}9", SNUMBER ) ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 11) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 1086;
                    ALOC = (ushort) ( Functions.Find( "." , SNUMBER ) ) ; 
                    __context__.SourceCodeLine = 1087;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ALOC == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1089;
                        MakeString ( SNUMBER , "{0}.", SNUMBER ) ; 
                        } 
                    
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 12) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 1094;
                    Functions.ClearBuffer ( SNUMBER ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_17__ == ( 13) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 1098;
                    ALOC = (ushort) ( Functions.Find( "." , SNUMBER ) ) ; 
                    __context__.SourceCodeLine = 1099;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ALOC != 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1101;
                        if ( Functions.TestForTrue  ( ( AIR  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 1103;
                            MakeString ( SCHANNELF , "{0:d2}", (short)Functions.Atoi( Functions.Left( SNUMBER , (int)( (ALOC - 1) ) ) )) ; 
                            __context__.SourceCodeLine = 1104;
                            MakeString ( SCHANNELR , "{0:d2}", (short)Functions.Atoi( Functions.Right( SNUMBER , (int)( (Functions.Length( SNUMBER ) - ALOC) ) ) )) ; 
                            __context__.SourceCodeLine = 1105;
                            MakeString ( SCHANNELTEMP , "DA2P{0}{1}\r", SCHANNELF , SCHANNELR ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1107;
                            if ( Functions.TestForTrue  ( ( CABLE  .Value)  ) ) 
                                { 
                                __context__.SourceCodeLine = 1109;
                                MakeString ( SCHANNELF , "{0:d3}", (short)Functions.Atoi( Functions.Left( SNUMBER , (int)( (ALOC - 1) ) ) )) ; 
                                __context__.SourceCodeLine = 1110;
                                MakeString ( SCHANNELR , "{0:d3}", (short)Functions.Atoi( Functions.Right( SNUMBER , (int)( (Functions.Length( SNUMBER ) - ALOC) ) ) )) ; 
                                __context__.SourceCodeLine = 1111;
                                MakeString ( SCHANNELTEMP , "DC2U{0} \rDC2L{1} \r", SCHANNELF , SCHANNELR ) ; 
                                } 
                            
                            }
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 1116;
                        if ( Functions.TestForTrue  ( ( AIR  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 1118;
                            MakeString ( SCHANNELF , "{0:d2}", (short)Functions.Atoi( SNUMBER )) ; 
                            __context__.SourceCodeLine = 1119;
                            MakeString ( SCHANNELTEMP , "DCCH{0}  \r", SCHANNELF ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1121;
                            if ( Functions.TestForTrue  ( ( CABLE  .Value)  ) ) 
                                { 
                                __context__.SourceCodeLine = 1123;
                                MakeString ( SCHANNELF , "{0:d3}", (short)Functions.Atoi( SNUMBER )) ; 
                                __context__.SourceCodeLine = 1124;
                                MakeString ( SCHANNELTEMP , "DCCH{0} \r", SCHANNELF ) ; 
                                } 
                            
                            }
                        
                        } 
                    
                    __context__.SourceCodeLine = 1127;
                    ICHANNELREQQUEUE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1128;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1130;
                        TO_DEVICE  .UpdateValue ( SCHANNELTEMP  ) ; 
                        __context__.SourceCodeLine = 1131;
                        ICOMMAND = (ushort) ( 12 ) ; 
                        __context__.SourceCodeLine = 1132;
                        IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1134;
                    Functions.ClearBuffer ( SNUMBER ) ; 
                    } 
                
                } 
                
            }
            
        
        __context__.SourceCodeLine = 1137;
        NUMBER__DOLLAR__  .UpdateValue ( SNUMBER  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IRKEYS_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1142;
        IIRKEYSQUEUE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1143;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IWAITINGFORRESPONSE  .Value == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 1145;
            TO_DEVICE  .UpdateValue ( SENDIRKEYS (  __context__ , (ushort)( IIRKEYSQUEUE ))  ) ; 
            __context__.SourceCodeLine = 1146;
            ICOMMAND = (ushort) ( 15 ) ; 
            __context__.SourceCodeLine = 1147;
            IIRKEYSSENT = (ushort) ( IIRKEYSQUEUE ) ; 
            __context__.SourceCodeLine = 1148;
            IWAITINGFORRESPONSE  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1159;
        IWAITINGFORRESPONSE  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1160;
        IPOWERQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1161;
        IINPUTQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1162;
        IAVMODEQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1163;
        IASPECTQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1164;
        IVOLUMEMUTEQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1165;
        ISLEEPQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1166;
        IEFFECT3DQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1167;
        IAUDIOMODEQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1168;
        ISURROUNDMODEQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1169;
        ICLOSEDCAPTIONQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1170;
        ICHANNELREQQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1171;
        ICHANNELUPQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1172;
        ICHANNELDOWNQUEUE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1173;
        IIRKEYSQUEUE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    STODEVICETEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    SNUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
    SCHANNELF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 9, this );
    SCHANNELR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 9, this );
    SCHANNELTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    
    RESPONSE_TIMEOUT = new Crestron.Logos.SplusObjects.DigitalInput( RESPONSE_TIMEOUT__DigitalInput__, this );
    m_DigitalInputList.Add( RESPONSE_TIMEOUT__DigitalInput__, RESPONSE_TIMEOUT );
    
    AUDIOMODE = new Crestron.Logos.SplusObjects.DigitalInput( AUDIOMODE__DigitalInput__, this );
    m_DigitalInputList.Add( AUDIOMODE__DigitalInput__, AUDIOMODE );
    
    SURROUNDMODE = new Crestron.Logos.SplusObjects.DigitalInput( SURROUNDMODE__DigitalInput__, this );
    m_DigitalInputList.Add( SURROUNDMODE__DigitalInput__, SURROUNDMODE );
    
    CLOSEDCAPTION = new Crestron.Logos.SplusObjects.DigitalInput( CLOSEDCAPTION__DigitalInput__, this );
    m_DigitalInputList.Add( CLOSEDCAPTION__DigitalInput__, CLOSEDCAPTION );
    
    CHANNELUP = new Crestron.Logos.SplusObjects.DigitalInput( CHANNELUP__DigitalInput__, this );
    m_DigitalInputList.Add( CHANNELUP__DigitalInput__, CHANNELUP );
    
    CHANNELDOWN = new Crestron.Logos.SplusObjects.DigitalInput( CHANNELDOWN__DigitalInput__, this );
    m_DigitalInputList.Add( CHANNELDOWN__DigitalInput__, CHANNELDOWN );
    
    AIR = new Crestron.Logos.SplusObjects.DigitalInput( AIR__DigitalInput__, this );
    m_DigitalInputList.Add( AIR__DigitalInput__, AIR );
    
    CABLE = new Crestron.Logos.SplusObjects.DigitalInput( CABLE__DigitalInput__, this );
    m_DigitalInputList.Add( CABLE__DigitalInput__, CABLE );
    
    KP = new InOutArray<DigitalInput>( 13, this );
    for( uint i = 0; i < 13; i++ )
    {
        KP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( KP__DigitalInput__ + i, KP__DigitalInput__, this );
        m_DigitalInputList.Add( KP__DigitalInput__ + i, KP[i+1] );
    }
    
    POWER = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        POWER[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( POWER__DigitalInput__ + i, POWER__DigitalInput__, this );
        m_DigitalInputList.Add( POWER__DigitalInput__ + i, POWER[i+1] );
    }
    
    INPUT = new InOutArray<DigitalInput>( 9, this );
    for( uint i = 0; i < 9; i++ )
    {
        INPUT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( INPUT__DigitalInput__ + i, INPUT__DigitalInput__, this );
        m_DigitalInputList.Add( INPUT__DigitalInput__ + i, INPUT[i+1] );
    }
    
    AVMODE = new InOutArray<DigitalInput>( 12, this );
    for( uint i = 0; i < 12; i++ )
    {
        AVMODE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( AVMODE__DigitalInput__ + i, AVMODE__DigitalInput__, this );
        m_DigitalInputList.Add( AVMODE__DigitalInput__ + i, AVMODE[i+1] );
    }
    
    ASPECT = new InOutArray<DigitalInput>( 11, this );
    for( uint i = 0; i < 11; i++ )
    {
        ASPECT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( ASPECT__DigitalInput__ + i, ASPECT__DigitalInput__, this );
        m_DigitalInputList.Add( ASPECT__DigitalInput__ + i, ASPECT[i+1] );
    }
    
    VOLUMEMUTE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOLUMEMUTE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTE__DigitalInput__ + i, VOLUMEMUTE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTE__DigitalInput__ + i, VOLUMEMUTE[i+1] );
    }
    
    SLEEP = new InOutArray<DigitalInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        SLEEP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SLEEP__DigitalInput__ + i, SLEEP__DigitalInput__, this );
        m_DigitalInputList.Add( SLEEP__DigitalInput__ + i, SLEEP[i+1] );
    }
    
    EFFECT3D = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        EFFECT3D[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( EFFECT3D__DigitalInput__ + i, EFFECT3D__DigitalInput__, this );
        m_DigitalInputList.Add( EFFECT3D__DigitalInput__ + i, EFFECT3D[i+1] );
    }
    
    POLL = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        POLL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__ + i, POLL__DigitalInput__, this );
        m_DigitalInputList.Add( POLL__DigitalInput__ + i, POLL[i+1] );
    }
    
    IRKEYS = new InOutArray<DigitalInput>( 54, this );
    for( uint i = 0; i < 54; i++ )
    {
        IRKEYS[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( IRKEYS__DigitalInput__ + i, IRKEYS__DigitalInput__, this );
        m_DigitalInputList.Add( IRKEYS__DigitalInput__ + i, IRKEYS[i+1] );
    }
    
    IWAITINGFORRESPONSE = new Crestron.Logos.SplusObjects.DigitalOutput( IWAITINGFORRESPONSE__DigitalOutput__, this );
    m_DigitalOutputList.Add( IWAITINGFORRESPONSE__DigitalOutput__, IWAITINGFORRESPONSE );
    
    CURRENT_POWER = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_POWER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_POWER__AnalogSerialOutput__, CURRENT_POWER );
    
    CURRENT_INPUT = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_INPUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_INPUT__AnalogSerialOutput__, CURRENT_INPUT );
    
    CURRENT_AVMODE = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_AVMODE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_AVMODE__AnalogSerialOutput__, CURRENT_AVMODE );
    
    CURRENT_ASPECT = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_ASPECT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_ASPECT__AnalogSerialOutput__, CURRENT_ASPECT );
    
    CURRENT_VOLUMEMUTE = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_VOLUMEMUTE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_VOLUMEMUTE__AnalogSerialOutput__, CURRENT_VOLUMEMUTE );
    
    CURRENT_SLEEP = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_SLEEP__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_SLEEP__AnalogSerialOutput__, CURRENT_SLEEP );
    
    CURRENT_EFFECT3D = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_EFFECT3D__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_EFFECT3D__AnalogSerialOutput__, CURRENT_EFFECT3D );
    
    CURRENT_VOLUME = new Crestron.Logos.SplusObjects.AnalogOutput( CURRENT_VOLUME__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CURRENT_VOLUME__AnalogSerialOutput__, CURRENT_VOLUME );
    
    FROM_DEVICE = new Crestron.Logos.SplusObjects.StringInput( FROM_DEVICE__AnalogSerialInput__, 25, this );
    m_StringInputList.Add( FROM_DEVICE__AnalogSerialInput__, FROM_DEVICE );
    
    NUMBER__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( NUMBER__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( NUMBER__DOLLAR____AnalogSerialOutput__, NUMBER__DOLLAR__ );
    
    TO_DEVICE = new Crestron.Logos.SplusObjects.StringOutput( TO_DEVICE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TO_DEVICE__AnalogSerialOutput__, TO_DEVICE );
    
    
    for( uint i = 0; i < 2; i++ )
        POWER[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( POWER_OnPush_0, false ) );
        
    for( uint i = 0; i < 9; i++ )
        INPUT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( INPUT_OnPush_1, false ) );
        
    for( uint i = 0; i < 12; i++ )
        AVMODE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( AVMODE_OnPush_2, false ) );
        
    for( uint i = 0; i < 11; i++ )
        ASPECT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( ASPECT_OnPush_3, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOLUMEMUTE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTE_OnPush_4, false ) );
        
    for( uint i = 0; i < 5; i++ )
        SLEEP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( SLEEP_OnPush_5, false ) );
        
    for( uint i = 0; i < 8; i++ )
        EFFECT3D[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( EFFECT3D_OnPush_6, false ) );
        
    for( uint i = 0; i < 8; i++ )
        POLL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_7, false ) );
        
    AUDIOMODE.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUDIOMODE_OnPush_8, false ) );
    SURROUNDMODE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SURROUNDMODE_OnPush_9, false ) );
    CLOSEDCAPTION.OnDigitalPush.Add( new InputChangeHandlerWrapper( CLOSEDCAPTION_OnPush_10, false ) );
    CHANNELUP.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHANNELUP_OnPush_11, false ) );
    CHANNELDOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( CHANNELDOWN_OnPush_12, false ) );
    FROM_DEVICE.OnSerialChange.Add( new InputChangeHandlerWrapper( FROM_DEVICE_OnChange_13, false ) );
    RESPONSE_TIMEOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( RESPONSE_TIMEOUT_OnPush_14, false ) );
    for( uint i = 0; i < 13; i++ )
        KP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( KP_OnPush_15, false ) );
        
    for( uint i = 0; i < 54; i++ )
        IRKEYS[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( IRKEYS_OnPush_16, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public CrestronModuleClass_SHARP_LC_70LE745U_IP_V1_0_PROCESSOR ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint RESPONSE_TIMEOUT__DigitalInput__ = 0;
const uint AUDIOMODE__DigitalInput__ = 1;
const uint SURROUNDMODE__DigitalInput__ = 2;
const uint CLOSEDCAPTION__DigitalInput__ = 3;
const uint CHANNELUP__DigitalInput__ = 4;
const uint CHANNELDOWN__DigitalInput__ = 5;
const uint AIR__DigitalInput__ = 6;
const uint CABLE__DigitalInput__ = 7;
const uint KP__DigitalInput__ = 8;
const uint POWER__DigitalInput__ = 21;
const uint INPUT__DigitalInput__ = 23;
const uint AVMODE__DigitalInput__ = 32;
const uint ASPECT__DigitalInput__ = 44;
const uint VOLUMEMUTE__DigitalInput__ = 55;
const uint SLEEP__DigitalInput__ = 57;
const uint EFFECT3D__DigitalInput__ = 62;
const uint POLL__DigitalInput__ = 70;
const uint IRKEYS__DigitalInput__ = 78;
const uint FROM_DEVICE__AnalogSerialInput__ = 0;
const uint IWAITINGFORRESPONSE__DigitalOutput__ = 0;
const uint CURRENT_POWER__AnalogSerialOutput__ = 0;
const uint CURRENT_INPUT__AnalogSerialOutput__ = 1;
const uint CURRENT_AVMODE__AnalogSerialOutput__ = 2;
const uint CURRENT_ASPECT__AnalogSerialOutput__ = 3;
const uint CURRENT_VOLUMEMUTE__AnalogSerialOutput__ = 4;
const uint CURRENT_SLEEP__AnalogSerialOutput__ = 5;
const uint CURRENT_EFFECT3D__AnalogSerialOutput__ = 6;
const uint CURRENT_VOLUME__AnalogSerialOutput__ = 7;
const uint NUMBER__DOLLAR____AnalogSerialOutput__ = 8;
const uint TO_DEVICE__AnalogSerialOutput__ = 9;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
