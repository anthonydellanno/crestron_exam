#ifndef __S2_ANALOG_INCREMENT_WITH_VARIABLE_LIMITS__DRIVER__H__
#define __S2_ANALOG_INCREMENT_WITH_VARIABLE_LIMITS__DRIVER__H__




/*
* Constructor and Destructor
*/

/*
* DIGITAL_INPUT
*/
#define __S2_Analog_Increment_with_Variable_Limits__driver__DIUP_DIG_INPUT 0
#define __S2_Analog_Increment_with_Variable_Limits__driver__DIDOWN_DIG_INPUT 1
#define __S2_Analog_Increment_with_Variable_Limits__driver__DIMUTE_DIG_INPUT 2


/*
* ANALOG_INPUT
*/
#define __S2_Analog_Increment_with_Variable_Limits__driver__AIINCREMENT_ANALOG_INPUT 0
#define __S2_Analog_Increment_with_Variable_Limits__driver__AILOWERLIMIT_ANALOG_INPUT 1
#define __S2_Analog_Increment_with_Variable_Limits__driver__AIUPPERLIMIT_ANALOG_INPUT 2
#define __S2_Analog_Increment_with_Variable_Limits__driver__AIMUTELEVEL_ANALOG_INPUT 3




/*
* DIGITAL_OUTPUT
*/


/*
* ANALOG_OUTPUT
*/
#define __S2_Analog_Increment_with_Variable_Limits__driver__AOLEVEL_ANALOG_OUTPUT 0



/*
* Direct Socket Variables
*/




/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* INTEGER_PARAMETER
*/
/*
* SIGNED_INTEGER_PARAMETER
*/
/*
* LONG_INTEGER_PARAMETER
*/
/*
* SIGNED_LONG_INTEGER_PARAMETER
*/
/*
* STRING_PARAMETER
*/


/*
* INTEGER
*/


/*
* LONG_INTEGER
*/


/*
* SIGNED_INTEGER
*/


/*
* SIGNED_LONG_INTEGER
*/


/*
* STRING
*/

/*
* STRUCTURE
*/

START_GLOBAL_VAR_STRUCT( S2_Analog_Increment_with_Variable_Limits__driver_ )
{
   void* InstancePtr;
   struct GenericOutputString_s sGenericOutStr;
   unsigned short LastModifiedArrayIndex;

   unsigned short __G_IPREVIOUSLEVEL;
   long __G_SLILOWERLIMIT;
};

START_NVRAM_VAR_STRUCT( S2_Analog_Increment_with_Variable_Limits__driver_ )
{
};



#endif //__S2_ANALOG_INCREMENT_WITH_VARIABLE_LIMITS__DRIVER__H__

