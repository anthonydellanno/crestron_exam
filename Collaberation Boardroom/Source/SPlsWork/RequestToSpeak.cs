using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_REQUESTTOSPEAK
{
    public class UserModuleClass_REQUESTTOSPEAK : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE1;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE2;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE3;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE4;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE5;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE6;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE7;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE8;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE9;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE10;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE11;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE12;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE13;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE14;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE15;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE16;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE17;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE18;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE19;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE20;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE21;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE22;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE23;
        Crestron.Logos.SplusObjects.DigitalInput BTNDELEGATE24;
        Crestron.Logos.SplusObjects.DigitalInput BTNADVANCESPEAKER;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION1;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION2;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION3;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION4;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION5;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION6;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION7;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION8;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION9;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION10;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION11;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION12;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION13;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION14;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION15;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION16;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION17;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION18;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION19;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION20;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION21;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION22;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION23;
        Crestron.Logos.SplusObjects.StringOutput RTSPOSITION24;
        Crestron.Logos.SplusObjects.AnalogOutput RTSQUEUELENGTH;
        SplusTcpClient DELEGATEIDENTITYSERVER;
        ushort DIS_STATUS = 0;
        ushort SERVERONLINE = 0;
        ushort NAMEDATAAVAILABLE = 0;
        ushort NAMEDATAVALID = 0;
        ushort CPUTEMPERROR = 0;
        ushort STORAGEERROR = 0;
        ushort POWERSUPPLYERROR = 0;
        ushort QUEUELENGTH = 0;
        CrestronString DELEGATES;
        CrestronString [] DELEGATESARRAY;
        CrestronString [] RTSQUEUE;
        private void UPDATETEXT (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 203;
            RTSPOSITION1  .UpdateValue ( RTSQUEUE [ 0 ]  ) ; 
            __context__.SourceCodeLine = 204;
            RTSPOSITION2  .UpdateValue ( RTSQUEUE [ 1 ]  ) ; 
            __context__.SourceCodeLine = 205;
            RTSPOSITION3  .UpdateValue ( RTSQUEUE [ 2 ]  ) ; 
            __context__.SourceCodeLine = 206;
            RTSPOSITION4  .UpdateValue ( RTSQUEUE [ 3 ]  ) ; 
            __context__.SourceCodeLine = 207;
            RTSPOSITION5  .UpdateValue ( RTSQUEUE [ 4 ]  ) ; 
            __context__.SourceCodeLine = 208;
            RTSPOSITION6  .UpdateValue ( RTSQUEUE [ 5 ]  ) ; 
            __context__.SourceCodeLine = 209;
            RTSPOSITION7  .UpdateValue ( RTSQUEUE [ 6 ]  ) ; 
            __context__.SourceCodeLine = 210;
            RTSPOSITION8  .UpdateValue ( RTSQUEUE [ 7 ]  ) ; 
            __context__.SourceCodeLine = 211;
            RTSPOSITION9  .UpdateValue ( RTSQUEUE [ 8 ]  ) ; 
            __context__.SourceCodeLine = 212;
            RTSPOSITION10  .UpdateValue ( RTSQUEUE [ 9 ]  ) ; 
            __context__.SourceCodeLine = 213;
            RTSPOSITION11  .UpdateValue ( RTSQUEUE [ 10 ]  ) ; 
            __context__.SourceCodeLine = 214;
            RTSPOSITION12  .UpdateValue ( RTSQUEUE [ 11 ]  ) ; 
            __context__.SourceCodeLine = 215;
            RTSPOSITION13  .UpdateValue ( RTSQUEUE [ 12 ]  ) ; 
            __context__.SourceCodeLine = 216;
            RTSPOSITION14  .UpdateValue ( RTSQUEUE [ 13 ]  ) ; 
            __context__.SourceCodeLine = 217;
            RTSPOSITION15  .UpdateValue ( RTSQUEUE [ 14 ]  ) ; 
            __context__.SourceCodeLine = 218;
            RTSPOSITION16  .UpdateValue ( RTSQUEUE [ 15 ]  ) ; 
            __context__.SourceCodeLine = 219;
            RTSPOSITION17  .UpdateValue ( RTSQUEUE [ 16 ]  ) ; 
            __context__.SourceCodeLine = 220;
            RTSPOSITION18  .UpdateValue ( RTSQUEUE [ 17 ]  ) ; 
            __context__.SourceCodeLine = 221;
            RTSPOSITION19  .UpdateValue ( RTSQUEUE [ 18 ]  ) ; 
            __context__.SourceCodeLine = 222;
            RTSPOSITION20  .UpdateValue ( RTSQUEUE [ 19 ]  ) ; 
            __context__.SourceCodeLine = 223;
            RTSPOSITION21  .UpdateValue ( RTSQUEUE [ 20 ]  ) ; 
            __context__.SourceCodeLine = 224;
            RTSPOSITION22  .UpdateValue ( RTSQUEUE [ 21 ]  ) ; 
            __context__.SourceCodeLine = 225;
            RTSPOSITION23  .UpdateValue ( RTSQUEUE [ 22 ]  ) ; 
            __context__.SourceCodeLine = 226;
            RTSPOSITION24  .UpdateValue ( RTSQUEUE [ 23 ]  ) ; 
            
            }
            
        private void UPDATEQUEUE (  SplusExecutionContext __context__, ushort X ) 
            { 
            
            __context__.SourceCodeLine = 231;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (X == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 232;
                QUEUELENGTH = (ushort) ( (QUEUELENGTH + 1) ) ; 
                __context__.SourceCodeLine = 233;
                RTSQUEUELENGTH  .Value = (ushort) ( QUEUELENGTH ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 234;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (X == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 235;
                    QUEUELENGTH = (ushort) ( (QUEUELENGTH - 1) ) ; 
                    __context__.SourceCodeLine = 236;
                    RTSQUEUELENGTH  .Value = (ushort) ( QUEUELENGTH ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 238;
            UPDATETEXT (  __context__  ) ; 
            
            }
            
        private void ADVANCESPEAKER (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 243;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)23; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 244;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (I == 23))  ) ) 
                    { 
                    __context__.SourceCodeLine = 245;
                    RTSQUEUE [ I ]  .UpdateValue ( "EMPTY"  ) ; 
                    __context__.SourceCodeLine = 246;
                    UPDATEQUEUE (  __context__ , (ushort)( 0 )) ; 
                    } 
                
                __context__.SourceCodeLine = 248;
                RTSQUEUE [ I ]  .UpdateValue ( RTSQUEUE [ (I + 1) ]  ) ; 
                __context__.SourceCodeLine = 243;
                } 
            
            __context__.SourceCodeLine = 250;
            ; 
            
            }
            
        private void ADDTOQUEUE (  SplusExecutionContext __context__, CrestronString SPEAKER ) 
            { 
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 256;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)23; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 257;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RTSQUEUE[ I ] == "EMPTY"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 258;
                    RTSQUEUE [ I ]  .UpdateValue ( SPEAKER  ) ; 
                    __context__.SourceCodeLine = 259;
                    UPDATEQUEUE (  __context__ , (ushort)( 1 )) ; 
                    __context__.SourceCodeLine = 260;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 256;
                } 
            
            
            }
            
        private void CHECKIFINQUEUE (  SplusExecutionContext __context__, CrestronString SPEAKER ) 
            { 
            ushort I = 0;
            
            ushort X = 0;
            
            
            __context__.SourceCodeLine = 269;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)23; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 271;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (RTSQUEUE[ I ] == SPEAKER))  ) ) 
                    { 
                    __context__.SourceCodeLine = 273;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (I == 23))  ) ) 
                        { 
                        __context__.SourceCodeLine = 274;
                        RTSQUEUE [ I ]  .UpdateValue ( "EMPTY"  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 277;
                        ushort __FN_FORSTART_VAL__2 = (ushort) ( I ) ;
                        ushort __FN_FOREND_VAL__2 = (ushort)23; 
                        int __FN_FORSTEP_VAL__2 = (int)1; 
                        for ( X  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (X  >= __FN_FORSTART_VAL__2) && (X  <= __FN_FOREND_VAL__2) ) : ( (X  <= __FN_FORSTART_VAL__2) && (X  >= __FN_FOREND_VAL__2) ) ; X  += (ushort)__FN_FORSTEP_VAL__2) 
                            { 
                            __context__.SourceCodeLine = 278;
                            RTSQUEUE [ X ]  .UpdateValue ( RTSQUEUE [ (X + 1) ]  ) ; 
                            __context__.SourceCodeLine = 277;
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 281;
                    UPDATEQUEUE (  __context__ , (ushort)( 0 )) ; 
                    __context__.SourceCodeLine = 282;
                    return ; 
                    } 
                
                __context__.SourceCodeLine = 269;
                } 
            
            
            }
            
        private void REQUESTTOSPEAK (  SplusExecutionContext __context__, CrestronString SPEAKER ) 
            { 
            
            __context__.SourceCodeLine = 288;
            CHECKIFINQUEUE (  __context__ , SPEAKER) ; 
            __context__.SourceCodeLine = 290;
            ADDTOQUEUE (  __context__ , SPEAKER) ; 
            
            }
            
        private void ADDDELEGATE (  SplusExecutionContext __context__, CrestronString SPEAKER ) 
            { 
            
            __context__.SourceCodeLine = 295;
            
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)(Functions.GetC( SPEAKER ) - 64));
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                        {
                        __context__.SourceCodeLine = 298;
                        DELEGATESARRAY [ 0 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                        {
                        __context__.SourceCodeLine = 299;
                        DELEGATESARRAY [ 1 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                        {
                        __context__.SourceCodeLine = 300;
                        DELEGATESARRAY [ 2 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                        {
                        __context__.SourceCodeLine = 301;
                        DELEGATESARRAY [ 3 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                        {
                        __context__.SourceCodeLine = 302;
                        DELEGATESARRAY [ 4 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                        {
                        __context__.SourceCodeLine = 303;
                        DELEGATESARRAY [ 5 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                        {
                        __context__.SourceCodeLine = 304;
                        DELEGATESARRAY [ 6 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) ) ) ) 
                        {
                        __context__.SourceCodeLine = 305;
                        DELEGATESARRAY [ 7 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 9) ) ) ) 
                        {
                        __context__.SourceCodeLine = 306;
                        DELEGATESARRAY [ 8 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 10) ) ) ) 
                        {
                        __context__.SourceCodeLine = 307;
                        DELEGATESARRAY [ 9 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 11) ) ) ) 
                        {
                        __context__.SourceCodeLine = 308;
                        DELEGATESARRAY [ 10 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 12) ) ) ) 
                        {
                        __context__.SourceCodeLine = 309;
                        DELEGATESARRAY [ 11 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 13) ) ) ) 
                        {
                        __context__.SourceCodeLine = 310;
                        DELEGATESARRAY [ 12 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 14) ) ) ) 
                        {
                        __context__.SourceCodeLine = 311;
                        DELEGATESARRAY [ 13 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 15) ) ) ) 
                        {
                        __context__.SourceCodeLine = 312;
                        DELEGATESARRAY [ 14 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 16) ) ) ) 
                        {
                        __context__.SourceCodeLine = 313;
                        DELEGATESARRAY [ 15 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 17) ) ) ) 
                        {
                        __context__.SourceCodeLine = 314;
                        DELEGATESARRAY [ 16 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 18) ) ) ) 
                        {
                        __context__.SourceCodeLine = 315;
                        DELEGATESARRAY [ 17 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 19) ) ) ) 
                        {
                        __context__.SourceCodeLine = 316;
                        DELEGATESARRAY [ 18 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 20) ) ) ) 
                        {
                        __context__.SourceCodeLine = 317;
                        DELEGATESARRAY [ 19 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 21) ) ) ) 
                        {
                        __context__.SourceCodeLine = 318;
                        DELEGATESARRAY [ 20 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 22) ) ) ) 
                        {
                        __context__.SourceCodeLine = 319;
                        DELEGATESARRAY [ 21 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 23) ) ) ) 
                        {
                        __context__.SourceCodeLine = 320;
                        DELEGATESARRAY [ 22 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 24) ) ) ) 
                        {
                        __context__.SourceCodeLine = 321;
                        DELEGATESARRAY [ 23 ]  .UpdateValue ( SPEAKER  ) ; 
                        }
                    
                    } 
                    
                }
                
            
            
            }
            
        private void GETDELEGATENAMES (  SplusExecutionContext __context__ ) 
            { 
            ushort I = 0;
            
            CrestronString CMD;
            CMD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 329;
            Trace( "{0}", "GETTING DELEGATE NAMES" ) ; 
            __context__.SourceCodeLine = 331;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)23; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 334;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( I <= 7 ) ) && Functions.TestForTrue ( Functions.BoolToInt (Bit( DELEGATES , (int)( 23 ) , (int)( I ) ) == 1) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 335;
                    CMD  .UpdateValue ( "get delegatename" + Functions.Chr (  (int) ( (I + 65) ) ) + "\u000D"  ) ; 
                    __context__.SourceCodeLine = 336;
                    Functions.SocketSend ( DELEGATEIDENTITYSERVER , CMD ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 338;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( I > 7 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( I <= 16 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (Bit( DELEGATES , (int)( 24 ) , (int)( (I - 8) ) ) == 1) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 339;
                        CMD  .UpdateValue ( "get delegatename" + Functions.Chr (  (int) ( (I + 65) ) ) + "\u000D"  ) ; 
                        __context__.SourceCodeLine = 340;
                        Functions.SocketSend ( DELEGATEIDENTITYSERVER , CMD ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 342;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( I > 16 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( I <= 23 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (Bit( DELEGATES , (int)( 25 ) , (int)( (I - 16) ) ) == 1) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 343;
                            CMD  .UpdateValue ( "get delegatename" + Functions.Chr (  (int) ( (I + 65) ) ) + "\u000D"  ) ; 
                            __context__.SourceCodeLine = 344;
                            Functions.SocketSend ( DELEGATEIDENTITYSERVER , CMD ) ; 
                            } 
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 331;
                } 
            
            
            }
            
        private void GETACTIVEDELEGATES (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 351;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SERVERONLINE == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (NAMEDATAAVAILABLE == 1) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (NAMEDATAVALID == 1) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (CPUTEMPERROR == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (STORAGEERROR == 0) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (POWERSUPPLYERROR == 0) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 352;
                Trace( "{0}", "GETTING ACTIVE DELEGATES" ) ; 
                __context__.SourceCodeLine = 354;
                Functions.SocketSend ( DELEGATEIDENTITYSERVER , "get activedelegates\u000D" ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 357;
                Trace( "{0}", "Server conditions not ideal" ) ; 
                } 
            
            
            }
            
        private void PROCESSRESPONSE (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            CrestronString REMOVED;
            REMOVED  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            CrestronString SPEAKER;
            SPEAKER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 367;
            Trace( "{0}{1}", "PROCESSING RESPONSE: " , RESPONSE ) ; 
            __context__.SourceCodeLine = 369;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( RESPONSE , (int)( 13 ) ) == "report status"))  ) ) 
                { 
                __context__.SourceCodeLine = 371;
                SERVERONLINE = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 0 ) ) ) ; 
                __context__.SourceCodeLine = 372;
                NAMEDATAAVAILABLE = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 1 ) ) ) ; 
                __context__.SourceCodeLine = 373;
                NAMEDATAVALID = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 2 ) ) ) ; 
                __context__.SourceCodeLine = 374;
                CPUTEMPERROR = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 3 ) ) ) ; 
                __context__.SourceCodeLine = 375;
                STORAGEERROR = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 4 ) ) ) ; 
                __context__.SourceCodeLine = 376;
                POWERSUPPLYERROR = (ushort) ( Bit( RESPONSE , (int)( 14 ) , (int)( 5 ) ) ) ; 
                __context__.SourceCodeLine = 378;
                GETACTIVEDELEGATES (  __context__  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 379;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( RESPONSE , (int)( 22 ) ) == "report activedelegates"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 381;
                    DELEGATES  .UpdateValue ( RESPONSE  ) ; 
                    __context__.SourceCodeLine = 383;
                    GETDELEGATENAMES (  __context__  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 384;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( RESPONSE , (int)( 19 ) ) == "report delegatename"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 386;
                        REMOVED  .UpdateValue ( Functions.Remove ( "report delegatename" , RESPONSE )  ) ; 
                        __context__.SourceCodeLine = 387;
                        SPEAKER  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 1 ) ,  (int) ( (Functions.Length( RESPONSE ) - 1) ) )  ) ; 
                        __context__.SourceCodeLine = 389;
                        ADDDELEGATE (  __context__ , SPEAKER) ; 
                        } 
                    
                    }
                
                }
            
            
            }
            
        object BTNDELEGATE1_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 427;
                REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 0 ]) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object BTNDELEGATE2_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 431;
            REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 1 ]) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object BTNDELEGATE3_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 435;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 2 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE4_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 439;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 3 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE5_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 443;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 4 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE6_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 447;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 5 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE7_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 451;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 6 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE8_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 455;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 7 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE9_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 459;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 8 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE10_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 463;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 9 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE11_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 467;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 10 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE12_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 471;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 11 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE13_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 475;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 12 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE14_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 479;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 13 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE15_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 483;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 14 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE16_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 487;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 15 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE17_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 491;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 16 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE18_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 495;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 17 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE19_OnPush_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 499;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 18 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE20_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 503;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 19 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE21_OnPush_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 507;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 20 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE22_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 511;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 21 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE23_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 515;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 22 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNDELEGATE24_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 519;
        REQUESTTOSPEAK (  __context__ , DELEGATESARRAY[ 23 ]) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNADVANCESPEAKER_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 523;
        ADVANCESPEAKER (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DELEGATEIDENTITYSERVER_OnSocketConnect_25 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 548;
        Trace( "{0}", "SOCKET CONNECT" ) ; 
        __context__.SourceCodeLine = 551;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DIS_STATUS == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 552;
            Trace( "{0}", "CONNECTED TO DIS" ) ; 
            __context__.SourceCodeLine = 555;
            Functions.SocketSend ( DELEGATEIDENTITYSERVER , "get status\u000D" ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object DELEGATEIDENTITYSERVER_OnSocketReceive_26 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 568;
        Trace( "{0}", "SOCKET RECEIVED" ) ; 
        __context__.SourceCodeLine = 571;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u00FE" , DELEGATEIDENTITYSERVER.SocketRxBuf ))  ) ) 
            { 
            __context__.SourceCodeLine = 573;
            PROCESSRESPONSE (  __context__ , Functions.Remove( "\u00FE" , DELEGATEIDENTITYSERVER.SocketRxBuf )) ; 
            __context__.SourceCodeLine = 571;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 594;
        Functions.SetArray ( RTSQUEUE , "EMPTY" ) ; 
        __context__.SourceCodeLine = 596;
        DIS_STATUS = (ushort) ( Functions.SocketConnectClient( DELEGATEIDENTITYSERVER , "192.168.20.16" , (ushort)( 20150 ) , (ushort)( 10 ) ) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    DELEGATES  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    DELEGATESARRAY  = new CrestronString[ 24 ];
    for( uint i = 0; i < 24; i++ )
        DELEGATESARRAY [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 80, this );
    RTSQUEUE  = new CrestronString[ 24 ];
    for( uint i = 0; i < 24; i++ )
        RTSQUEUE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 80, this );
    DELEGATEIDENTITYSERVER  = new SplusTcpClient ( 1024, this );
    
    BTNDELEGATE1 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE1__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE1__DigitalInput__, BTNDELEGATE1 );
    
    BTNDELEGATE2 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE2__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE2__DigitalInput__, BTNDELEGATE2 );
    
    BTNDELEGATE3 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE3__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE3__DigitalInput__, BTNDELEGATE3 );
    
    BTNDELEGATE4 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE4__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE4__DigitalInput__, BTNDELEGATE4 );
    
    BTNDELEGATE5 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE5__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE5__DigitalInput__, BTNDELEGATE5 );
    
    BTNDELEGATE6 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE6__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE6__DigitalInput__, BTNDELEGATE6 );
    
    BTNDELEGATE7 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE7__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE7__DigitalInput__, BTNDELEGATE7 );
    
    BTNDELEGATE8 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE8__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE8__DigitalInput__, BTNDELEGATE8 );
    
    BTNDELEGATE9 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE9__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE9__DigitalInput__, BTNDELEGATE9 );
    
    BTNDELEGATE10 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE10__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE10__DigitalInput__, BTNDELEGATE10 );
    
    BTNDELEGATE11 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE11__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE11__DigitalInput__, BTNDELEGATE11 );
    
    BTNDELEGATE12 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE12__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE12__DigitalInput__, BTNDELEGATE12 );
    
    BTNDELEGATE13 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE13__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE13__DigitalInput__, BTNDELEGATE13 );
    
    BTNDELEGATE14 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE14__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE14__DigitalInput__, BTNDELEGATE14 );
    
    BTNDELEGATE15 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE15__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE15__DigitalInput__, BTNDELEGATE15 );
    
    BTNDELEGATE16 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE16__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE16__DigitalInput__, BTNDELEGATE16 );
    
    BTNDELEGATE17 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE17__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE17__DigitalInput__, BTNDELEGATE17 );
    
    BTNDELEGATE18 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE18__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE18__DigitalInput__, BTNDELEGATE18 );
    
    BTNDELEGATE19 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE19__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE19__DigitalInput__, BTNDELEGATE19 );
    
    BTNDELEGATE20 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE20__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE20__DigitalInput__, BTNDELEGATE20 );
    
    BTNDELEGATE21 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE21__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE21__DigitalInput__, BTNDELEGATE21 );
    
    BTNDELEGATE22 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE22__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE22__DigitalInput__, BTNDELEGATE22 );
    
    BTNDELEGATE23 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE23__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE23__DigitalInput__, BTNDELEGATE23 );
    
    BTNDELEGATE24 = new Crestron.Logos.SplusObjects.DigitalInput( BTNDELEGATE24__DigitalInput__, this );
    m_DigitalInputList.Add( BTNDELEGATE24__DigitalInput__, BTNDELEGATE24 );
    
    BTNADVANCESPEAKER = new Crestron.Logos.SplusObjects.DigitalInput( BTNADVANCESPEAKER__DigitalInput__, this );
    m_DigitalInputList.Add( BTNADVANCESPEAKER__DigitalInput__, BTNADVANCESPEAKER );
    
    RTSQUEUELENGTH = new Crestron.Logos.SplusObjects.AnalogOutput( RTSQUEUELENGTH__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( RTSQUEUELENGTH__AnalogSerialOutput__, RTSQUEUELENGTH );
    
    RTSPOSITION1 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION1__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION1__AnalogSerialOutput__, RTSPOSITION1 );
    
    RTSPOSITION2 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION2__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION2__AnalogSerialOutput__, RTSPOSITION2 );
    
    RTSPOSITION3 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION3__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION3__AnalogSerialOutput__, RTSPOSITION3 );
    
    RTSPOSITION4 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION4__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION4__AnalogSerialOutput__, RTSPOSITION4 );
    
    RTSPOSITION5 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION5__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION5__AnalogSerialOutput__, RTSPOSITION5 );
    
    RTSPOSITION6 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION6__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION6__AnalogSerialOutput__, RTSPOSITION6 );
    
    RTSPOSITION7 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION7__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION7__AnalogSerialOutput__, RTSPOSITION7 );
    
    RTSPOSITION8 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION8__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION8__AnalogSerialOutput__, RTSPOSITION8 );
    
    RTSPOSITION9 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION9__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION9__AnalogSerialOutput__, RTSPOSITION9 );
    
    RTSPOSITION10 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION10__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION10__AnalogSerialOutput__, RTSPOSITION10 );
    
    RTSPOSITION11 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION11__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION11__AnalogSerialOutput__, RTSPOSITION11 );
    
    RTSPOSITION12 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION12__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION12__AnalogSerialOutput__, RTSPOSITION12 );
    
    RTSPOSITION13 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION13__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION13__AnalogSerialOutput__, RTSPOSITION13 );
    
    RTSPOSITION14 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION14__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION14__AnalogSerialOutput__, RTSPOSITION14 );
    
    RTSPOSITION15 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION15__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION15__AnalogSerialOutput__, RTSPOSITION15 );
    
    RTSPOSITION16 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION16__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION16__AnalogSerialOutput__, RTSPOSITION16 );
    
    RTSPOSITION17 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION17__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION17__AnalogSerialOutput__, RTSPOSITION17 );
    
    RTSPOSITION18 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION18__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION18__AnalogSerialOutput__, RTSPOSITION18 );
    
    RTSPOSITION19 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION19__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION19__AnalogSerialOutput__, RTSPOSITION19 );
    
    RTSPOSITION20 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION20__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION20__AnalogSerialOutput__, RTSPOSITION20 );
    
    RTSPOSITION21 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION21__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION21__AnalogSerialOutput__, RTSPOSITION21 );
    
    RTSPOSITION22 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION22__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION22__AnalogSerialOutput__, RTSPOSITION22 );
    
    RTSPOSITION23 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION23__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION23__AnalogSerialOutput__, RTSPOSITION23 );
    
    RTSPOSITION24 = new Crestron.Logos.SplusObjects.StringOutput( RTSPOSITION24__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RTSPOSITION24__AnalogSerialOutput__, RTSPOSITION24 );
    
    
    BTNDELEGATE1.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE1_OnPush_0, false ) );
    BTNDELEGATE2.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE2_OnPush_1, false ) );
    BTNDELEGATE3.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE3_OnPush_2, false ) );
    BTNDELEGATE4.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE4_OnPush_3, false ) );
    BTNDELEGATE5.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE5_OnPush_4, false ) );
    BTNDELEGATE6.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE6_OnPush_5, false ) );
    BTNDELEGATE7.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE7_OnPush_6, false ) );
    BTNDELEGATE8.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE8_OnPush_7, false ) );
    BTNDELEGATE9.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE9_OnPush_8, false ) );
    BTNDELEGATE10.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE10_OnPush_9, false ) );
    BTNDELEGATE11.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE11_OnPush_10, false ) );
    BTNDELEGATE12.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE12_OnPush_11, false ) );
    BTNDELEGATE13.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE13_OnPush_12, false ) );
    BTNDELEGATE14.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE14_OnPush_13, false ) );
    BTNDELEGATE15.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE15_OnPush_14, false ) );
    BTNDELEGATE16.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE16_OnPush_15, false ) );
    BTNDELEGATE17.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE17_OnPush_16, false ) );
    BTNDELEGATE18.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE18_OnPush_17, false ) );
    BTNDELEGATE19.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE19_OnPush_18, false ) );
    BTNDELEGATE20.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE20_OnPush_19, false ) );
    BTNDELEGATE21.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE21_OnPush_20, false ) );
    BTNDELEGATE22.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE22_OnPush_21, false ) );
    BTNDELEGATE23.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE23_OnPush_22, false ) );
    BTNDELEGATE24.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNDELEGATE24_OnPush_23, false ) );
    BTNADVANCESPEAKER.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNADVANCESPEAKER_OnPush_24, false ) );
    DELEGATEIDENTITYSERVER.OnSocketConnect.Add( new SocketHandlerWrapper( DELEGATEIDENTITYSERVER_OnSocketConnect_25, false ) );
    DELEGATEIDENTITYSERVER.OnSocketReceive.Add( new SocketHandlerWrapper( DELEGATEIDENTITYSERVER_OnSocketReceive_26, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_REQUESTTOSPEAK ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint BTNDELEGATE1__DigitalInput__ = 0;
const uint BTNDELEGATE2__DigitalInput__ = 1;
const uint BTNDELEGATE3__DigitalInput__ = 2;
const uint BTNDELEGATE4__DigitalInput__ = 3;
const uint BTNDELEGATE5__DigitalInput__ = 4;
const uint BTNDELEGATE6__DigitalInput__ = 5;
const uint BTNDELEGATE7__DigitalInput__ = 6;
const uint BTNDELEGATE8__DigitalInput__ = 7;
const uint BTNDELEGATE9__DigitalInput__ = 8;
const uint BTNDELEGATE10__DigitalInput__ = 9;
const uint BTNDELEGATE11__DigitalInput__ = 10;
const uint BTNDELEGATE12__DigitalInput__ = 11;
const uint BTNDELEGATE13__DigitalInput__ = 12;
const uint BTNDELEGATE14__DigitalInput__ = 13;
const uint BTNDELEGATE15__DigitalInput__ = 14;
const uint BTNDELEGATE16__DigitalInput__ = 15;
const uint BTNDELEGATE17__DigitalInput__ = 16;
const uint BTNDELEGATE18__DigitalInput__ = 17;
const uint BTNDELEGATE19__DigitalInput__ = 18;
const uint BTNDELEGATE20__DigitalInput__ = 19;
const uint BTNDELEGATE21__DigitalInput__ = 20;
const uint BTNDELEGATE22__DigitalInput__ = 21;
const uint BTNDELEGATE23__DigitalInput__ = 22;
const uint BTNDELEGATE24__DigitalInput__ = 23;
const uint BTNADVANCESPEAKER__DigitalInput__ = 24;
const uint RTSPOSITION1__AnalogSerialOutput__ = 0;
const uint RTSPOSITION2__AnalogSerialOutput__ = 1;
const uint RTSPOSITION3__AnalogSerialOutput__ = 2;
const uint RTSPOSITION4__AnalogSerialOutput__ = 3;
const uint RTSPOSITION5__AnalogSerialOutput__ = 4;
const uint RTSPOSITION6__AnalogSerialOutput__ = 5;
const uint RTSPOSITION7__AnalogSerialOutput__ = 6;
const uint RTSPOSITION8__AnalogSerialOutput__ = 7;
const uint RTSPOSITION9__AnalogSerialOutput__ = 8;
const uint RTSPOSITION10__AnalogSerialOutput__ = 9;
const uint RTSPOSITION11__AnalogSerialOutput__ = 10;
const uint RTSPOSITION12__AnalogSerialOutput__ = 11;
const uint RTSPOSITION13__AnalogSerialOutput__ = 12;
const uint RTSPOSITION14__AnalogSerialOutput__ = 13;
const uint RTSPOSITION15__AnalogSerialOutput__ = 14;
const uint RTSPOSITION16__AnalogSerialOutput__ = 15;
const uint RTSPOSITION17__AnalogSerialOutput__ = 16;
const uint RTSPOSITION18__AnalogSerialOutput__ = 17;
const uint RTSPOSITION19__AnalogSerialOutput__ = 18;
const uint RTSPOSITION20__AnalogSerialOutput__ = 19;
const uint RTSPOSITION21__AnalogSerialOutput__ = 20;
const uint RTSPOSITION22__AnalogSerialOutput__ = 21;
const uint RTSPOSITION23__AnalogSerialOutput__ = 22;
const uint RTSPOSITION24__AnalogSerialOutput__ = 23;
const uint RTSQUEUELENGTH__AnalogSerialOutput__ = 24;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
