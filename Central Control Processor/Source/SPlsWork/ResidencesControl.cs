using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_RESIDENCESCONTROL
{
    public class UserModuleClass_RESIDENCESCONTROL : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE1;
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE2;
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE3;
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE4;
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE5;
        Crestron.Logos.SplusObjects.DigitalInput BTNRESIDENCE6;
        Crestron.Logos.SplusObjects.DigitalInput RESIDENCESPOLLING;
        Crestron.Logos.SplusObjects.StringOutput ACTIVERESIDENCENUMBER;
        Crestron.Logos.SplusObjects.DigitalOutput RES1BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES1KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES1APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES1BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES1BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES1KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES1LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.DigitalOutput RES2BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES2KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES2APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES2BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES2BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES2KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES2LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.DigitalOutput RES3BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES3KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES3APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES3BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES3BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES3KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES3LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.DigitalOutput RES4BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES4KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES4APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES4BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES4BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES4KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES4LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.DigitalOutput RES5BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES5KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES5APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES5BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES5BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES5KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES5LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.DigitalOutput RES6BATHROOMTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES6KITCHENTV;
        Crestron.Logos.SplusObjects.DigitalOutput RES6APPLESOURCE;
        Crestron.Logos.SplusObjects.StringOutput RES6BATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES6BEDROMNAME;
        Crestron.Logos.SplusObjects.StringOutput RES6KITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput RES6LIVINGROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYBATHROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYBEDROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYKITCHENNAME;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYLIVINGROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYBATHROOMTV;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYKITCHENTV;
        Crestron.Logos.SplusObjects.StringOutput DISPLAYAPPLESOURCE;
        ushort ACTIVERESIDENCE = 0;
        ushort BATHROOMTV = 0;
        ushort KITCHENTV = 0;
        ushort RESIDENCE1BATHROOMTV = 0;
        ushort RESIDENCE1KITCHENTV = 0;
        ushort RESIDENCE1APPLESOURCE = 0;
        CrestronString RESIDENCE1BATHROOMNAME;
        CrestronString RESIDENCE1BEDROMNAME;
        CrestronString RESIDENCE1KITCHENNAME;
        CrestronString RESIDENCE1LIVINGROOMNAME;
        ushort RESIDENCE2BATHROOMTV = 0;
        ushort RESIDENCE2KITCHENTV = 0;
        ushort RESIDENCE2APPLESOURCE = 0;
        CrestronString RESIDENCE2BATHROOMNAME;
        CrestronString RESIDENCE2BEDROMNAME;
        CrestronString RESIDENCE2KITCHENNAME;
        CrestronString RESIDENCE2LIVINGROOMNAME;
        ushort RESIDENCE3BATHROOMTV = 0;
        ushort RESIDENCE3KITCHENTV = 0;
        ushort RESIDENCE3APPLESOURCE = 0;
        CrestronString RESIDENCE3BATHROOMNAME;
        CrestronString RESIDENCE3BEDROMNAME;
        CrestronString RESIDENCE3KITCHENNAME;
        CrestronString RESIDENCE3LIVINGROOMNAME;
        ushort RESIDENCE4BATHROOMTV = 0;
        ushort RESIDENCE4KITCHENTV = 0;
        ushort RESIDENCE4APPLESOURCE = 0;
        CrestronString RESIDENCE4BATHROOMNAME;
        CrestronString RESIDENCE4BEDROMNAME;
        CrestronString RESIDENCE4KITCHENNAME;
        CrestronString RESIDENCE4LIVINGROOMNAME;
        ushort RESIDENCE5BATHROOMTV = 0;
        ushort RESIDENCE5KITCHENTV = 0;
        ushort RESIDENCE5APPLESOURCE = 0;
        CrestronString RESIDENCE5BATHROOMNAME;
        CrestronString RESIDENCE5BEDROMNAME;
        CrestronString RESIDENCE5KITCHENNAME;
        CrestronString RESIDENCE5LIVINGROOMNAME;
        ushort RESIDENCE6BATHROOMTV = 0;
        ushort RESIDENCE6KITCHENTV = 0;
        ushort RESIDENCE6APPLESOURCE = 0;
        CrestronString RESIDENCE6BATHROOMNAME;
        CrestronString RESIDENCE6BEDROMNAME;
        CrestronString RESIDENCE6KITCHENNAME;
        CrestronString RESIDENCE6LIVINGROOMNAME;
        private CrestronString DIGITALTOSTRING (  SplusExecutionContext __context__, ushort X ) 
            { 
            
            __context__.SourceCodeLine = 259;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (X == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 260;
                return ( "Yes" ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 261;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (X == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 262;
                    return ( "No" ) ; 
                    } 
                
                }
            
            
            return ""; // default return value (none specified in module)
            }
            
        private void DISPLAYRESIDENCECONFIGINFO (  SplusExecutionContext __context__, ushort RESIDENCE ) 
            { 
            
            __context__.SourceCodeLine = 269;
            
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)RESIDENCE);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 272;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE1BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 273;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE1BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 274;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE1KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 275;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE1LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 276;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE1BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 277;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE1KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 278;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE1APPLESOURCE ))  ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 281;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE2BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 282;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE2BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 283;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE2KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 284;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE2LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 285;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE2BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 286;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE2KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 287;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE2APPLESOURCE ))  ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 290;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE3BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 291;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE3BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 292;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE3KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 293;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE3LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 294;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE3BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 295;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE3KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 296;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE3APPLESOURCE ))  ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 299;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE4BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 300;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE4BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 301;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE4KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 302;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE4LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 303;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE4BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 304;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE4KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 305;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE4APPLESOURCE ))  ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 308;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE5BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 309;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE5BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 310;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE5KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 311;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE5LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 312;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE5BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 313;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE5KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 314;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE5APPLESOURCE ))  ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                        { 
                        __context__.SourceCodeLine = 317;
                        DISPLAYBATHROOMNAME  .UpdateValue ( RESIDENCE6BATHROOMNAME  ) ; 
                        __context__.SourceCodeLine = 318;
                        DISPLAYBEDROOMNAME  .UpdateValue ( RESIDENCE6BEDROMNAME  ) ; 
                        __context__.SourceCodeLine = 319;
                        DISPLAYKITCHENNAME  .UpdateValue ( RESIDENCE6KITCHENNAME  ) ; 
                        __context__.SourceCodeLine = 320;
                        DISPLAYLIVINGROOMNAME  .UpdateValue ( RESIDENCE6LIVINGROOMNAME  ) ; 
                        __context__.SourceCodeLine = 321;
                        DISPLAYBATHROOMTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE6BATHROOMTV ))  ) ; 
                        __context__.SourceCodeLine = 322;
                        DISPLAYKITCHENTV  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE6KITCHENTV ))  ) ; 
                        __context__.SourceCodeLine = 323;
                        DISPLAYAPPLESOURCE  .UpdateValue ( DIGITALTOSTRING (  __context__ , (ushort)( RESIDENCE6APPLESOURCE ))  ) ; 
                        } 
                    
                    } 
                    
                }
                
            
            
            }
            
        private void SETACTIVERESIDENCE (  SplusExecutionContext __context__, ushort RESIDENCE ) 
            { 
            
            __context__.SourceCodeLine = 330;
            ACTIVERESIDENCE = (ushort) ( RESIDENCE ) ; 
            __context__.SourceCodeLine = 332;
            ACTIVERESIDENCENUMBER  .UpdateValue ( Functions.ItoA (  (int) ( RESIDENCE ) )  ) ; 
            __context__.SourceCodeLine = 334;
            DISPLAYRESIDENCECONFIGINFO (  __context__ , (ushort)( RESIDENCE )) ; 
            
            }
            
        private void SETNEWBATHROOMNAME (  SplusExecutionContext __context__ ) 
            { 
            short NFILEHANDLE = 0;
            
            CrestronString SBUF;
            SBUF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4096, this );
            
            
            __context__.SourceCodeLine = 342;
            Trace( "set new bathroom name firing") ; 
            __context__.SourceCodeLine = 343;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 344;
            NFILEHANDLE = (short) ( FileOpen( "\\NVRAM\\residencesconfig.txt" ,(ushort) 0 ) ) ; 
            __context__.SourceCodeLine = 346;
            Trace( "{0}{1:d}", "error code: " , (short)NFILEHANDLE) ; 
            __context__.SourceCodeLine = 348;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( FileRead( (short)( NFILEHANDLE ) , SBUF , (ushort)( 4096 ) ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 350;
                Trace( "Read from file: {0}\r\n", SBUF ) ; 
                __context__.SourceCodeLine = 352;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FileClose( (short)( NFILEHANDLE ) ) != 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 354;
                    Trace( "Error closing file\r\n") ; 
                    } 
                
                __context__.SourceCodeLine = 348;
                } 
            
            __context__.SourceCodeLine = 359;
            EndFileOperations ( ) ; 
            
            }
            
        private void SETRESIDENCE1OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 364;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 366;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 366;
                RESIDENCE1BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 367;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 367;
                RESIDENCE1KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 368;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 368;
                RESIDENCE1BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 369;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 369;
                RESIDENCE1BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 370;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 370;
                RESIDENCE1KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 371;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 371;
                RESIDENCE1LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 372;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 372;
                RESIDENCE1APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 374;
            RES1BATHROOMTV  .Value = (ushort) ( RESIDENCE1BATHROOMTV ) ; 
            __context__.SourceCodeLine = 375;
            RES1KITCHENTV  .Value = (ushort) ( RESIDENCE1KITCHENTV ) ; 
            __context__.SourceCodeLine = 376;
            RES1APPLESOURCE  .Value = (ushort) ( RESIDENCE1APPLESOURCE ) ; 
            __context__.SourceCodeLine = 377;
            RES1BATHROOMNAME  .UpdateValue ( RESIDENCE1BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 378;
            RES1BEDROMNAME  .UpdateValue ( RESIDENCE1BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 379;
            RES1KITCHENNAME  .UpdateValue ( RESIDENCE1KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 380;
            RES1LIVINGROOMNAME  .UpdateValue ( RESIDENCE1LIVINGROOMNAME  ) ; 
            
            }
            
        private void SETRESIDENCE2OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 385;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 387;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 387;
                RESIDENCE2BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 388;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 388;
                RESIDENCE2KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 389;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 389;
                RESIDENCE2BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 390;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 390;
                RESIDENCE2BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 391;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 391;
                RESIDENCE2KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 392;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 392;
                RESIDENCE2LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 393;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 393;
                RESIDENCE2APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 395;
            RES2BATHROOMTV  .Value = (ushort) ( RESIDENCE2BATHROOMTV ) ; 
            __context__.SourceCodeLine = 396;
            RES2KITCHENTV  .Value = (ushort) ( RESIDENCE2KITCHENTV ) ; 
            __context__.SourceCodeLine = 397;
            RES2APPLESOURCE  .Value = (ushort) ( RESIDENCE2APPLESOURCE ) ; 
            __context__.SourceCodeLine = 398;
            RES2BATHROOMNAME  .UpdateValue ( RESIDENCE2BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 399;
            RES2BEDROMNAME  .UpdateValue ( RESIDENCE2BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 400;
            RES2KITCHENNAME  .UpdateValue ( RESIDENCE2KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 401;
            RES2LIVINGROOMNAME  .UpdateValue ( RESIDENCE2LIVINGROOMNAME  ) ; 
            
            }
            
        private void SETRESIDENCE3OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 405;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 407;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 407;
                RESIDENCE3BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 408;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 408;
                RESIDENCE3KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 409;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 409;
                RESIDENCE3BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 410;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 410;
                RESIDENCE3BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 411;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 411;
                RESIDENCE3KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 412;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 412;
                RESIDENCE3LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 413;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 413;
                RESIDENCE3APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 415;
            RES3BATHROOMTV  .Value = (ushort) ( RESIDENCE3BATHROOMTV ) ; 
            __context__.SourceCodeLine = 416;
            RES3KITCHENTV  .Value = (ushort) ( RESIDENCE3KITCHENTV ) ; 
            __context__.SourceCodeLine = 417;
            RES3APPLESOURCE  .Value = (ushort) ( RESIDENCE3APPLESOURCE ) ; 
            __context__.SourceCodeLine = 418;
            RES3BATHROOMNAME  .UpdateValue ( RESIDENCE3BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 419;
            RES3BEDROMNAME  .UpdateValue ( RESIDENCE3BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 420;
            RES3KITCHENNAME  .UpdateValue ( RESIDENCE3KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 421;
            RES3LIVINGROOMNAME  .UpdateValue ( RESIDENCE3LIVINGROOMNAME  ) ; 
            
            }
            
        private void SETRESIDENCE4OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 426;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 428;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 428;
                RESIDENCE4BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 429;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 429;
                RESIDENCE4KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 430;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 430;
                RESIDENCE4BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 431;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 431;
                RESIDENCE4BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 432;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 432;
                RESIDENCE4KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 433;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 433;
                RESIDENCE4LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 434;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 434;
                RESIDENCE4APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 436;
            RES4BATHROOMTV  .Value = (ushort) ( RESIDENCE4BATHROOMTV ) ; 
            __context__.SourceCodeLine = 437;
            RES4KITCHENTV  .Value = (ushort) ( RESIDENCE4KITCHENTV ) ; 
            __context__.SourceCodeLine = 438;
            RES4APPLESOURCE  .Value = (ushort) ( RESIDENCE4APPLESOURCE ) ; 
            __context__.SourceCodeLine = 439;
            RES4BATHROOMNAME  .UpdateValue ( RESIDENCE4BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 440;
            RES4BEDROMNAME  .UpdateValue ( RESIDENCE4BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 441;
            RES4KITCHENNAME  .UpdateValue ( RESIDENCE4KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 442;
            RES4LIVINGROOMNAME  .UpdateValue ( RESIDENCE4LIVINGROOMNAME  ) ; 
            
            }
            
        private void SETRESIDENCE5OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 448;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 450;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 450;
                RESIDENCE5BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 451;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 451;
                RESIDENCE5KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 452;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 452;
                RESIDENCE5BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 453;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 453;
                RESIDENCE5BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 454;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 454;
                RESIDENCE5KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 455;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 455;
                RESIDENCE5LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 456;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 456;
                RESIDENCE5APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 458;
            RES5BATHROOMTV  .Value = (ushort) ( RESIDENCE5BATHROOMTV ) ; 
            __context__.SourceCodeLine = 459;
            RES5KITCHENTV  .Value = (ushort) ( RESIDENCE5KITCHENTV ) ; 
            __context__.SourceCodeLine = 460;
            RES5APPLESOURCE  .Value = (ushort) ( RESIDENCE5APPLESOURCE ) ; 
            __context__.SourceCodeLine = 461;
            RES5BATHROOMNAME  .UpdateValue ( RESIDENCE5BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 462;
            RES5BEDROMNAME  .UpdateValue ( RESIDENCE5BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 463;
            RES5KITCHENNAME  .UpdateValue ( RESIDENCE5KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 464;
            RES5LIVINGROOMNAME  .UpdateValue ( RESIDENCE5LIVINGROOMNAME  ) ; 
            
            }
            
        private void SETRESIDENCE6OUTPUTS (  SplusExecutionContext __context__, CrestronString RESPONSE ) 
            { 
            
            __context__.SourceCodeLine = 469;
            RESPONSE  .UpdateValue ( Functions.Left ( RESPONSE ,  (int) ( (Functions.Length( RESPONSE ) - 2) ) )  ) ; 
            __context__.SourceCodeLine = 471;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 471;
                RESIDENCE6BATHROOMTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 16 ) , (int)( 16 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 472;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_TV" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 472;
                RESIDENCE6KITCHENTV = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 15 ) , (int)( 15 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 473;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BATHROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 473;
                RESIDENCE6BATHROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 18 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 474;
            if ( Functions.TestForTrue  ( ( Functions.Find( "BEDROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 474;
                RESIDENCE6BEDROMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 17 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 475;
            if ( Functions.TestForTrue  ( ( Functions.Find( "KITCHEN_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 475;
                RESIDENCE6KITCHENNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 16 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 476;
            if ( Functions.TestForTrue  ( ( Functions.Find( "LIVING_ROOM_NAME" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 476;
                RESIDENCE6LIVINGROOMNAME  .UpdateValue ( Functions.Mid ( RESPONSE ,  (int) ( 21 ) ,  (int) ( Functions.Length( RESPONSE ) ) )  ) ; 
                }
            
            __context__.SourceCodeLine = 477;
            if ( Functions.TestForTrue  ( ( Functions.Find( "APPLE_SOURCE" , RESPONSE ))  ) ) 
                {
                __context__.SourceCodeLine = 477;
                RESIDENCE6APPLESOURCE = (ushort) ( Functions.Atoi( Functions.Mid( RESPONSE , (int)( 17 ) , (int)( 17 ) ) ) ) ; 
                }
            
            __context__.SourceCodeLine = 479;
            RES6BATHROOMTV  .Value = (ushort) ( RESIDENCE6BATHROOMTV ) ; 
            __context__.SourceCodeLine = 480;
            RES6KITCHENTV  .Value = (ushort) ( RESIDENCE6KITCHENTV ) ; 
            __context__.SourceCodeLine = 481;
            RES6APPLESOURCE  .Value = (ushort) ( RESIDENCE6APPLESOURCE ) ; 
            __context__.SourceCodeLine = 482;
            RES6BATHROOMNAME  .UpdateValue ( RESIDENCE6BATHROOMNAME  ) ; 
            __context__.SourceCodeLine = 483;
            RES6BEDROMNAME  .UpdateValue ( RESIDENCE6BEDROMNAME  ) ; 
            __context__.SourceCodeLine = 484;
            RES6KITCHENNAME  .UpdateValue ( RESIDENCE6KITCHENNAME  ) ; 
            __context__.SourceCodeLine = 485;
            RES6LIVINGROOMNAME  .UpdateValue ( RESIDENCE6LIVINGROOMNAME  ) ; 
            
            }
            
        private void PROCESSRESPONSE (  SplusExecutionContext __context__, CrestronString RESPONSE , ushort RESIDENCE ) 
            { 
            ushort FIRSTCHAR = 0;
            
            
            __context__.SourceCodeLine = 491;
            FIRSTCHAR = (ushort) ( Functions.Atoi( Functions.Left( RESPONSE , (int)( 1 ) ) ) ) ; 
            __context__.SourceCodeLine = 493;
            Trace( "{0}{1:d}", "FIRST CHAR: " , (short)FIRSTCHAR) ; 
            __context__.SourceCodeLine = 495;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (FIRSTCHAR == RESIDENCE))  ) ) 
                { 
                __context__.SourceCodeLine = 496;
                
                    {
                    int __SPLS_TMPVAR__SWTCH_2__ = ((int)FIRSTCHAR);
                    
                        { 
                        if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 1) ) ) ) 
                            {
                            __context__.SourceCodeLine = 498;
                            SETRESIDENCE1OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 2) ) ) ) 
                            {
                            __context__.SourceCodeLine = 499;
                            SETRESIDENCE2OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 3) ) ) ) 
                            {
                            __context__.SourceCodeLine = 500;
                            SETRESIDENCE3OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 4) ) ) ) 
                            {
                            __context__.SourceCodeLine = 501;
                            SETRESIDENCE4OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 5) ) ) ) 
                            {
                            __context__.SourceCodeLine = 502;
                            SETRESIDENCE5OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_2__ == ( 6) ) ) ) 
                            {
                            __context__.SourceCodeLine = 503;
                            SETRESIDENCE6OUTPUTS (  __context__ , RESPONSE) ; 
                            }
                        
                        } 
                        
                    }
                    
                
                } 
            
            
            }
            
        private void GETROOMCONFIG (  SplusExecutionContext __context__ ) 
            { 
            short FILE = 0;
            
            CrestronString SBUF;
            SBUF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4096, this );
            
            ushort I = 0;
            
            
            __context__.SourceCodeLine = 513;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)6; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 515;
                StartFileOperations ( ) ; 
                __context__.SourceCodeLine = 516;
                FILE = (short) ( FileOpen( "\\NVRAM\\c5.txt" ,(ushort) 0 ) ) ; 
                __context__.SourceCodeLine = 518;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( FILE >= 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 520;
                    while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( FileRead( (short)( FILE ) , SBUF , (ushort)( 4096 ) ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 522;
                        Trace( "Read from file: {0}", SBUF ) ; 
                        __context__.SourceCodeLine = 524;
                        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , SBUF ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 526;
                            PROCESSRESPONSE (  __context__ , Functions.Remove( "\u000D\u000A" , SBUF ), (ushort)( I )) ; 
                            __context__.SourceCodeLine = 524;
                            } 
                        
                        __context__.SourceCodeLine = 520;
                        } 
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 530;
                    Trace( "{0}{1:d}", "Error code: " , (short)FILE) ; 
                    } 
                
                __context__.SourceCodeLine = 532;
                EndFileOperations ( ) ; 
                __context__.SourceCodeLine = 513;
                } 
            
            
            }
            
        object BTNRESIDENCE1_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 571;
                SETACTIVERESIDENCE (  __context__ , (ushort)( 1 )) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object BTNRESIDENCE2_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 575;
            SETACTIVERESIDENCE (  __context__ , (ushort)( 2 )) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object BTNRESIDENCE3_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 579;
        SETACTIVERESIDENCE (  __context__ , (ushort)( 3 )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNRESIDENCE4_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 583;
        SETACTIVERESIDENCE (  __context__ , (ushort)( 4 )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNRESIDENCE5_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 587;
        SETACTIVERESIDENCE (  __context__ , (ushort)( 5 )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BTNRESIDENCE6_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 591;
        SETACTIVERESIDENCE (  __context__ , (ushort)( 6 )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RESIDENCESPOLLING_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 595;
        GETROOMCONFIG (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 656;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 658;
        GETROOMCONFIG (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    RESIDENCE1BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE1BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE1KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE1LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE2BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE2BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE2KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE2LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE3BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE3BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE3KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE3LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE4BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE4BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE4KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE4LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE5BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE5BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE5KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE5LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE6BATHROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE6BEDROMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE6KITCHENNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    RESIDENCE6LIVINGROOMNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
    
    BTNRESIDENCE1 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE1__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE1__DigitalInput__, BTNRESIDENCE1 );
    
    BTNRESIDENCE2 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE2__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE2__DigitalInput__, BTNRESIDENCE2 );
    
    BTNRESIDENCE3 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE3__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE3__DigitalInput__, BTNRESIDENCE3 );
    
    BTNRESIDENCE4 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE4__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE4__DigitalInput__, BTNRESIDENCE4 );
    
    BTNRESIDENCE5 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE5__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE5__DigitalInput__, BTNRESIDENCE5 );
    
    BTNRESIDENCE6 = new Crestron.Logos.SplusObjects.DigitalInput( BTNRESIDENCE6__DigitalInput__, this );
    m_DigitalInputList.Add( BTNRESIDENCE6__DigitalInput__, BTNRESIDENCE6 );
    
    RESIDENCESPOLLING = new Crestron.Logos.SplusObjects.DigitalInput( RESIDENCESPOLLING__DigitalInput__, this );
    m_DigitalInputList.Add( RESIDENCESPOLLING__DigitalInput__, RESIDENCESPOLLING );
    
    RES1BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES1BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES1BATHROOMTV__DigitalOutput__, RES1BATHROOMTV );
    
    RES1KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES1KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES1KITCHENTV__DigitalOutput__, RES1KITCHENTV );
    
    RES1APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES1APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES1APPLESOURCE__DigitalOutput__, RES1APPLESOURCE );
    
    RES2BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES2BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES2BATHROOMTV__DigitalOutput__, RES2BATHROOMTV );
    
    RES2KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES2KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES2KITCHENTV__DigitalOutput__, RES2KITCHENTV );
    
    RES2APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES2APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES2APPLESOURCE__DigitalOutput__, RES2APPLESOURCE );
    
    RES3BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES3BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES3BATHROOMTV__DigitalOutput__, RES3BATHROOMTV );
    
    RES3KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES3KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES3KITCHENTV__DigitalOutput__, RES3KITCHENTV );
    
    RES3APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES3APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES3APPLESOURCE__DigitalOutput__, RES3APPLESOURCE );
    
    RES4BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES4BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES4BATHROOMTV__DigitalOutput__, RES4BATHROOMTV );
    
    RES4KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES4KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES4KITCHENTV__DigitalOutput__, RES4KITCHENTV );
    
    RES4APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES4APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES4APPLESOURCE__DigitalOutput__, RES4APPLESOURCE );
    
    RES5BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES5BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES5BATHROOMTV__DigitalOutput__, RES5BATHROOMTV );
    
    RES5KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES5KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES5KITCHENTV__DigitalOutput__, RES5KITCHENTV );
    
    RES5APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES5APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES5APPLESOURCE__DigitalOutput__, RES5APPLESOURCE );
    
    RES6BATHROOMTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES6BATHROOMTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES6BATHROOMTV__DigitalOutput__, RES6BATHROOMTV );
    
    RES6KITCHENTV = new Crestron.Logos.SplusObjects.DigitalOutput( RES6KITCHENTV__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES6KITCHENTV__DigitalOutput__, RES6KITCHENTV );
    
    RES6APPLESOURCE = new Crestron.Logos.SplusObjects.DigitalOutput( RES6APPLESOURCE__DigitalOutput__, this );
    m_DigitalOutputList.Add( RES6APPLESOURCE__DigitalOutput__, RES6APPLESOURCE );
    
    ACTIVERESIDENCENUMBER = new Crestron.Logos.SplusObjects.StringOutput( ACTIVERESIDENCENUMBER__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ACTIVERESIDENCENUMBER__AnalogSerialOutput__, ACTIVERESIDENCENUMBER );
    
    RES1BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES1BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES1BATHROOMNAME__AnalogSerialOutput__, RES1BATHROOMNAME );
    
    RES1BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES1BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES1BEDROMNAME__AnalogSerialOutput__, RES1BEDROMNAME );
    
    RES1KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES1KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES1KITCHENNAME__AnalogSerialOutput__, RES1KITCHENNAME );
    
    RES1LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES1LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES1LIVINGROOMNAME__AnalogSerialOutput__, RES1LIVINGROOMNAME );
    
    RES2BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES2BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES2BATHROOMNAME__AnalogSerialOutput__, RES2BATHROOMNAME );
    
    RES2BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES2BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES2BEDROMNAME__AnalogSerialOutput__, RES2BEDROMNAME );
    
    RES2KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES2KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES2KITCHENNAME__AnalogSerialOutput__, RES2KITCHENNAME );
    
    RES2LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES2LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES2LIVINGROOMNAME__AnalogSerialOutput__, RES2LIVINGROOMNAME );
    
    RES3BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES3BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES3BATHROOMNAME__AnalogSerialOutput__, RES3BATHROOMNAME );
    
    RES3BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES3BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES3BEDROMNAME__AnalogSerialOutput__, RES3BEDROMNAME );
    
    RES3KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES3KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES3KITCHENNAME__AnalogSerialOutput__, RES3KITCHENNAME );
    
    RES3LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES3LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES3LIVINGROOMNAME__AnalogSerialOutput__, RES3LIVINGROOMNAME );
    
    RES4BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES4BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES4BATHROOMNAME__AnalogSerialOutput__, RES4BATHROOMNAME );
    
    RES4BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES4BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES4BEDROMNAME__AnalogSerialOutput__, RES4BEDROMNAME );
    
    RES4KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES4KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES4KITCHENNAME__AnalogSerialOutput__, RES4KITCHENNAME );
    
    RES4LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES4LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES4LIVINGROOMNAME__AnalogSerialOutput__, RES4LIVINGROOMNAME );
    
    RES5BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES5BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES5BATHROOMNAME__AnalogSerialOutput__, RES5BATHROOMNAME );
    
    RES5BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES5BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES5BEDROMNAME__AnalogSerialOutput__, RES5BEDROMNAME );
    
    RES5KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES5KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES5KITCHENNAME__AnalogSerialOutput__, RES5KITCHENNAME );
    
    RES5LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES5LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES5LIVINGROOMNAME__AnalogSerialOutput__, RES5LIVINGROOMNAME );
    
    RES6BATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES6BATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES6BATHROOMNAME__AnalogSerialOutput__, RES6BATHROOMNAME );
    
    RES6BEDROMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES6BEDROMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES6BEDROMNAME__AnalogSerialOutput__, RES6BEDROMNAME );
    
    RES6KITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( RES6KITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES6KITCHENNAME__AnalogSerialOutput__, RES6KITCHENNAME );
    
    RES6LIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( RES6LIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( RES6LIVINGROOMNAME__AnalogSerialOutput__, RES6LIVINGROOMNAME );
    
    DISPLAYBATHROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYBATHROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYBATHROOMNAME__AnalogSerialOutput__, DISPLAYBATHROOMNAME );
    
    DISPLAYBEDROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYBEDROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYBEDROOMNAME__AnalogSerialOutput__, DISPLAYBEDROOMNAME );
    
    DISPLAYKITCHENNAME = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYKITCHENNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYKITCHENNAME__AnalogSerialOutput__, DISPLAYKITCHENNAME );
    
    DISPLAYLIVINGROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYLIVINGROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYLIVINGROOMNAME__AnalogSerialOutput__, DISPLAYLIVINGROOMNAME );
    
    DISPLAYBATHROOMTV = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYBATHROOMTV__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYBATHROOMTV__AnalogSerialOutput__, DISPLAYBATHROOMTV );
    
    DISPLAYKITCHENTV = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYKITCHENTV__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYKITCHENTV__AnalogSerialOutput__, DISPLAYKITCHENTV );
    
    DISPLAYAPPLESOURCE = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYAPPLESOURCE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DISPLAYAPPLESOURCE__AnalogSerialOutput__, DISPLAYAPPLESOURCE );
    
    
    BTNRESIDENCE1.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE1_OnPush_0, false ) );
    BTNRESIDENCE2.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE2_OnPush_1, false ) );
    BTNRESIDENCE3.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE3_OnPush_2, false ) );
    BTNRESIDENCE4.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE4_OnPush_3, false ) );
    BTNRESIDENCE5.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE5_OnPush_4, false ) );
    BTNRESIDENCE6.OnDigitalPush.Add( new InputChangeHandlerWrapper( BTNRESIDENCE6_OnPush_5, false ) );
    RESIDENCESPOLLING.OnDigitalPush.Add( new InputChangeHandlerWrapper( RESIDENCESPOLLING_OnPush_6, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_RESIDENCESCONTROL ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint BTNRESIDENCE1__DigitalInput__ = 0;
const uint BTNRESIDENCE2__DigitalInput__ = 1;
const uint BTNRESIDENCE3__DigitalInput__ = 2;
const uint BTNRESIDENCE4__DigitalInput__ = 3;
const uint BTNRESIDENCE5__DigitalInput__ = 4;
const uint BTNRESIDENCE6__DigitalInput__ = 5;
const uint RESIDENCESPOLLING__DigitalInput__ = 6;
const uint ACTIVERESIDENCENUMBER__AnalogSerialOutput__ = 0;
const uint RES1BATHROOMTV__DigitalOutput__ = 0;
const uint RES1KITCHENTV__DigitalOutput__ = 1;
const uint RES1APPLESOURCE__DigitalOutput__ = 2;
const uint RES1BATHROOMNAME__AnalogSerialOutput__ = 1;
const uint RES1BEDROMNAME__AnalogSerialOutput__ = 2;
const uint RES1KITCHENNAME__AnalogSerialOutput__ = 3;
const uint RES1LIVINGROOMNAME__AnalogSerialOutput__ = 4;
const uint RES2BATHROOMTV__DigitalOutput__ = 3;
const uint RES2KITCHENTV__DigitalOutput__ = 4;
const uint RES2APPLESOURCE__DigitalOutput__ = 5;
const uint RES2BATHROOMNAME__AnalogSerialOutput__ = 5;
const uint RES2BEDROMNAME__AnalogSerialOutput__ = 6;
const uint RES2KITCHENNAME__AnalogSerialOutput__ = 7;
const uint RES2LIVINGROOMNAME__AnalogSerialOutput__ = 8;
const uint RES3BATHROOMTV__DigitalOutput__ = 6;
const uint RES3KITCHENTV__DigitalOutput__ = 7;
const uint RES3APPLESOURCE__DigitalOutput__ = 8;
const uint RES3BATHROOMNAME__AnalogSerialOutput__ = 9;
const uint RES3BEDROMNAME__AnalogSerialOutput__ = 10;
const uint RES3KITCHENNAME__AnalogSerialOutput__ = 11;
const uint RES3LIVINGROOMNAME__AnalogSerialOutput__ = 12;
const uint RES4BATHROOMTV__DigitalOutput__ = 9;
const uint RES4KITCHENTV__DigitalOutput__ = 10;
const uint RES4APPLESOURCE__DigitalOutput__ = 11;
const uint RES4BATHROOMNAME__AnalogSerialOutput__ = 13;
const uint RES4BEDROMNAME__AnalogSerialOutput__ = 14;
const uint RES4KITCHENNAME__AnalogSerialOutput__ = 15;
const uint RES4LIVINGROOMNAME__AnalogSerialOutput__ = 16;
const uint RES5BATHROOMTV__DigitalOutput__ = 12;
const uint RES5KITCHENTV__DigitalOutput__ = 13;
const uint RES5APPLESOURCE__DigitalOutput__ = 14;
const uint RES5BATHROOMNAME__AnalogSerialOutput__ = 17;
const uint RES5BEDROMNAME__AnalogSerialOutput__ = 18;
const uint RES5KITCHENNAME__AnalogSerialOutput__ = 19;
const uint RES5LIVINGROOMNAME__AnalogSerialOutput__ = 20;
const uint RES6BATHROOMTV__DigitalOutput__ = 15;
const uint RES6KITCHENTV__DigitalOutput__ = 16;
const uint RES6APPLESOURCE__DigitalOutput__ = 17;
const uint RES6BATHROOMNAME__AnalogSerialOutput__ = 21;
const uint RES6BEDROMNAME__AnalogSerialOutput__ = 22;
const uint RES6KITCHENNAME__AnalogSerialOutput__ = 23;
const uint RES6LIVINGROOMNAME__AnalogSerialOutput__ = 24;
const uint DISPLAYBATHROOMNAME__AnalogSerialOutput__ = 25;
const uint DISPLAYBEDROOMNAME__AnalogSerialOutput__ = 26;
const uint DISPLAYKITCHENNAME__AnalogSerialOutput__ = 27;
const uint DISPLAYLIVINGROOMNAME__AnalogSerialOutput__ = 28;
const uint DISPLAYBATHROOMTV__AnalogSerialOutput__ = 29;
const uint DISPLAYKITCHENTV__AnalogSerialOutput__ = 30;
const uint DISPLAYAPPLESOURCE__AnalogSerialOutput__ = 31;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
