using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_SHARP_XG_C465X_PROCESSOR_V1_0
{
    public class UserModuleClass_SHARP_XG_C465X_PROCESSOR_V1_0 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput COMPUTER_1_CONTRAST_POLL;
        Crestron.Logos.SplusObjects.DigitalInput COMPUTER_1_BRIGHTNESS_POLL;
        Crestron.Logos.SplusObjects.DigitalInput COMPUTER_2_CONTRAST_POLL;
        Crestron.Logos.SplusObjects.DigitalInput COMPUTER_2_BRIGHTNESS_POLL;
        Crestron.Logos.SplusObjects.DigitalInput DVI_CONTRAST_POLL;
        Crestron.Logos.SplusObjects.DigitalInput DVI_BRIGHTNESS_POLL;
        Crestron.Logos.SplusObjects.DigitalInput VIDEO_CONTRAST_POLL;
        Crestron.Logos.SplusObjects.DigitalInput VIDEO_BRIGHTNESS_POLL;
        Crestron.Logos.SplusObjects.DigitalInput S_VIDEO_CONTRAST_POLL;
        Crestron.Logos.SplusObjects.DigitalInput S_VIDEO_BRIGHTNESS_POLL;
        Crestron.Logos.SplusObjects.DigitalInput LAMP_HOURS_POLL;
        Crestron.Logos.SplusObjects.StringInput FROM_DEVICE;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_1_CONTRAST_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_1_CONTRAST_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_1_BRIGHTNESS_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_1_BRIGHTNESS_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_2_CONTRAST_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_2_CONTRAST_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_2_BRIGHTNESS_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput COMPUTER_2_BRIGHTNESS_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput DVI_CONTRAST_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput DVI_CONTRAST_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput DVI_BRIGHTNESS_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput DVI_BRIGHTNESS_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput VIDEO_CONTRAST_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput VIDEO_CONTRAST_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput VIDEO_BRIGHTNESS_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput VIDEO_BRIGHTNESS_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput S_VIDEO_CONTRAST_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput S_VIDEO_CONTRAST_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput S_VIDEO_BRIGHTNESS_VALUE;
        Crestron.Logos.SplusObjects.AnalogOutput S_VIDEO_BRIGHTNESS_LEVEL;
        Crestron.Logos.SplusObjects.AnalogOutput LAMP_HOURS;
        short ATEMP = 0;
        object FROM_DEVICE_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 217;
                ATEMP = (short) ( Functions.Atoi( FROM_DEVICE ) ) ; 
                __context__.SourceCodeLine = 218;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , FROM_DEVICE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 220;
                    ATEMP = (short) ( Functions.ToSignedInteger( -( ATEMP ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 222;
                if ( Functions.TestForTrue  ( ( COMPUTER_1_CONTRAST_POLL  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 224;
                    COMPUTER_1_CONTRAST_VALUE  .Value = (ushort) ( ATEMP ) ; 
                    __context__.SourceCodeLine = 225;
                    COMPUTER_1_CONTRAST_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 227;
                    if ( Functions.TestForTrue  ( ( COMPUTER_1_BRIGHTNESS_POLL  .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 229;
                        COMPUTER_1_BRIGHTNESS_VALUE  .Value = (ushort) ( ATEMP ) ; 
                        __context__.SourceCodeLine = 230;
                        COMPUTER_1_BRIGHTNESS_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 232;
                        if ( Functions.TestForTrue  ( ( COMPUTER_2_CONTRAST_POLL  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 234;
                            COMPUTER_2_CONTRAST_VALUE  .Value = (ushort) ( ATEMP ) ; 
                            __context__.SourceCodeLine = 235;
                            COMPUTER_2_CONTRAST_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 237;
                            if ( Functions.TestForTrue  ( ( COMPUTER_2_BRIGHTNESS_POLL  .Value)  ) ) 
                                { 
                                __context__.SourceCodeLine = 239;
                                COMPUTER_2_BRIGHTNESS_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                __context__.SourceCodeLine = 240;
                                COMPUTER_2_BRIGHTNESS_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 242;
                                if ( Functions.TestForTrue  ( ( DVI_CONTRAST_POLL  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 244;
                                    DVI_CONTRAST_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                    __context__.SourceCodeLine = 245;
                                    DVI_CONTRAST_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 247;
                                    if ( Functions.TestForTrue  ( ( DVI_BRIGHTNESS_POLL  .Value)  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 249;
                                        DVI_BRIGHTNESS_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                        __context__.SourceCodeLine = 250;
                                        DVI_BRIGHTNESS_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 252;
                                        if ( Functions.TestForTrue  ( ( VIDEO_CONTRAST_POLL  .Value)  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 254;
                                            VIDEO_CONTRAST_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                            __context__.SourceCodeLine = 255;
                                            VIDEO_CONTRAST_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 257;
                                            if ( Functions.TestForTrue  ( ( VIDEO_BRIGHTNESS_POLL  .Value)  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 259;
                                                VIDEO_BRIGHTNESS_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                                __context__.SourceCodeLine = 260;
                                                VIDEO_BRIGHTNESS_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 262;
                                                if ( Functions.TestForTrue  ( ( S_VIDEO_CONTRAST_POLL  .Value)  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 264;
                                                    S_VIDEO_CONTRAST_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                                    __context__.SourceCodeLine = 265;
                                                    S_VIDEO_CONTRAST_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 267;
                                                    if ( Functions.TestForTrue  ( ( S_VIDEO_BRIGHTNESS_POLL  .Value)  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 269;
                                                        S_VIDEO_BRIGHTNESS_VALUE  .Value = (ushort) ( ATEMP ) ; 
                                                        __context__.SourceCodeLine = 270;
                                                        S_VIDEO_BRIGHTNESS_LEVEL  .Value = (ushort) ( (ATEMP + 30) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 272;
                                                        if ( Functions.TestForTrue  ( ( LAMP_HOURS_POLL  .Value)  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 274;
                                                            LAMP_HOURS  .Value = (ushort) ( ATEMP ) ; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        COMPUTER_1_CONTRAST_POLL = new Crestron.Logos.SplusObjects.DigitalInput( COMPUTER_1_CONTRAST_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( COMPUTER_1_CONTRAST_POLL__DigitalInput__, COMPUTER_1_CONTRAST_POLL );
        
        COMPUTER_1_BRIGHTNESS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( COMPUTER_1_BRIGHTNESS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( COMPUTER_1_BRIGHTNESS_POLL__DigitalInput__, COMPUTER_1_BRIGHTNESS_POLL );
        
        COMPUTER_2_CONTRAST_POLL = new Crestron.Logos.SplusObjects.DigitalInput( COMPUTER_2_CONTRAST_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( COMPUTER_2_CONTRAST_POLL__DigitalInput__, COMPUTER_2_CONTRAST_POLL );
        
        COMPUTER_2_BRIGHTNESS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( COMPUTER_2_BRIGHTNESS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( COMPUTER_2_BRIGHTNESS_POLL__DigitalInput__, COMPUTER_2_BRIGHTNESS_POLL );
        
        DVI_CONTRAST_POLL = new Crestron.Logos.SplusObjects.DigitalInput( DVI_CONTRAST_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( DVI_CONTRAST_POLL__DigitalInput__, DVI_CONTRAST_POLL );
        
        DVI_BRIGHTNESS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( DVI_BRIGHTNESS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( DVI_BRIGHTNESS_POLL__DigitalInput__, DVI_BRIGHTNESS_POLL );
        
        VIDEO_CONTRAST_POLL = new Crestron.Logos.SplusObjects.DigitalInput( VIDEO_CONTRAST_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( VIDEO_CONTRAST_POLL__DigitalInput__, VIDEO_CONTRAST_POLL );
        
        VIDEO_BRIGHTNESS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( VIDEO_BRIGHTNESS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( VIDEO_BRIGHTNESS_POLL__DigitalInput__, VIDEO_BRIGHTNESS_POLL );
        
        S_VIDEO_CONTRAST_POLL = new Crestron.Logos.SplusObjects.DigitalInput( S_VIDEO_CONTRAST_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( S_VIDEO_CONTRAST_POLL__DigitalInput__, S_VIDEO_CONTRAST_POLL );
        
        S_VIDEO_BRIGHTNESS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( S_VIDEO_BRIGHTNESS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( S_VIDEO_BRIGHTNESS_POLL__DigitalInput__, S_VIDEO_BRIGHTNESS_POLL );
        
        LAMP_HOURS_POLL = new Crestron.Logos.SplusObjects.DigitalInput( LAMP_HOURS_POLL__DigitalInput__, this );
        m_DigitalInputList.Add( LAMP_HOURS_POLL__DigitalInput__, LAMP_HOURS_POLL );
        
        COMPUTER_1_CONTRAST_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_1_CONTRAST_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_1_CONTRAST_VALUE__AnalogSerialOutput__, COMPUTER_1_CONTRAST_VALUE );
        
        COMPUTER_1_CONTRAST_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_1_CONTRAST_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_1_CONTRAST_LEVEL__AnalogSerialOutput__, COMPUTER_1_CONTRAST_LEVEL );
        
        COMPUTER_1_BRIGHTNESS_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_1_BRIGHTNESS_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_1_BRIGHTNESS_VALUE__AnalogSerialOutput__, COMPUTER_1_BRIGHTNESS_VALUE );
        
        COMPUTER_1_BRIGHTNESS_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_1_BRIGHTNESS_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_1_BRIGHTNESS_LEVEL__AnalogSerialOutput__, COMPUTER_1_BRIGHTNESS_LEVEL );
        
        COMPUTER_2_CONTRAST_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_2_CONTRAST_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_2_CONTRAST_VALUE__AnalogSerialOutput__, COMPUTER_2_CONTRAST_VALUE );
        
        COMPUTER_2_CONTRAST_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_2_CONTRAST_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_2_CONTRAST_LEVEL__AnalogSerialOutput__, COMPUTER_2_CONTRAST_LEVEL );
        
        COMPUTER_2_BRIGHTNESS_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_2_BRIGHTNESS_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_2_BRIGHTNESS_VALUE__AnalogSerialOutput__, COMPUTER_2_BRIGHTNESS_VALUE );
        
        COMPUTER_2_BRIGHTNESS_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( COMPUTER_2_BRIGHTNESS_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( COMPUTER_2_BRIGHTNESS_LEVEL__AnalogSerialOutput__, COMPUTER_2_BRIGHTNESS_LEVEL );
        
        DVI_CONTRAST_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( DVI_CONTRAST_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( DVI_CONTRAST_VALUE__AnalogSerialOutput__, DVI_CONTRAST_VALUE );
        
        DVI_CONTRAST_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( DVI_CONTRAST_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( DVI_CONTRAST_LEVEL__AnalogSerialOutput__, DVI_CONTRAST_LEVEL );
        
        DVI_BRIGHTNESS_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( DVI_BRIGHTNESS_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( DVI_BRIGHTNESS_VALUE__AnalogSerialOutput__, DVI_BRIGHTNESS_VALUE );
        
        DVI_BRIGHTNESS_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( DVI_BRIGHTNESS_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( DVI_BRIGHTNESS_LEVEL__AnalogSerialOutput__, DVI_BRIGHTNESS_LEVEL );
        
        VIDEO_CONTRAST_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEO_CONTRAST_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( VIDEO_CONTRAST_VALUE__AnalogSerialOutput__, VIDEO_CONTRAST_VALUE );
        
        VIDEO_CONTRAST_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__, VIDEO_CONTRAST_LEVEL );
        
        VIDEO_BRIGHTNESS_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__, VIDEO_BRIGHTNESS_VALUE );
        
        VIDEO_BRIGHTNESS_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__, VIDEO_BRIGHTNESS_LEVEL );
        
        S_VIDEO_CONTRAST_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( S_VIDEO_CONTRAST_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( S_VIDEO_CONTRAST_VALUE__AnalogSerialOutput__, S_VIDEO_CONTRAST_VALUE );
        
        S_VIDEO_CONTRAST_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( S_VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( S_VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__, S_VIDEO_CONTRAST_LEVEL );
        
        S_VIDEO_BRIGHTNESS_VALUE = new Crestron.Logos.SplusObjects.AnalogOutput( S_VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( S_VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__, S_VIDEO_BRIGHTNESS_VALUE );
        
        S_VIDEO_BRIGHTNESS_LEVEL = new Crestron.Logos.SplusObjects.AnalogOutput( S_VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( S_VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__, S_VIDEO_BRIGHTNESS_LEVEL );
        
        LAMP_HOURS = new Crestron.Logos.SplusObjects.AnalogOutput( LAMP_HOURS__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( LAMP_HOURS__AnalogSerialOutput__, LAMP_HOURS );
        
        FROM_DEVICE = new Crestron.Logos.SplusObjects.StringInput( FROM_DEVICE__AnalogSerialInput__, 4, this );
        m_StringInputList.Add( FROM_DEVICE__AnalogSerialInput__, FROM_DEVICE );
        
        
        FROM_DEVICE.OnSerialChange.Add( new InputChangeHandlerWrapper( FROM_DEVICE_OnChange_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_SHARP_XG_C465X_PROCESSOR_V1_0 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint COMPUTER_1_CONTRAST_POLL__DigitalInput__ = 0;
    const uint COMPUTER_1_BRIGHTNESS_POLL__DigitalInput__ = 1;
    const uint COMPUTER_2_CONTRAST_POLL__DigitalInput__ = 2;
    const uint COMPUTER_2_BRIGHTNESS_POLL__DigitalInput__ = 3;
    const uint DVI_CONTRAST_POLL__DigitalInput__ = 4;
    const uint DVI_BRIGHTNESS_POLL__DigitalInput__ = 5;
    const uint VIDEO_CONTRAST_POLL__DigitalInput__ = 6;
    const uint VIDEO_BRIGHTNESS_POLL__DigitalInput__ = 7;
    const uint S_VIDEO_CONTRAST_POLL__DigitalInput__ = 8;
    const uint S_VIDEO_BRIGHTNESS_POLL__DigitalInput__ = 9;
    const uint LAMP_HOURS_POLL__DigitalInput__ = 10;
    const uint FROM_DEVICE__AnalogSerialInput__ = 0;
    const uint COMPUTER_1_CONTRAST_VALUE__AnalogSerialOutput__ = 0;
    const uint COMPUTER_1_CONTRAST_LEVEL__AnalogSerialOutput__ = 1;
    const uint COMPUTER_1_BRIGHTNESS_VALUE__AnalogSerialOutput__ = 2;
    const uint COMPUTER_1_BRIGHTNESS_LEVEL__AnalogSerialOutput__ = 3;
    const uint COMPUTER_2_CONTRAST_VALUE__AnalogSerialOutput__ = 4;
    const uint COMPUTER_2_CONTRAST_LEVEL__AnalogSerialOutput__ = 5;
    const uint COMPUTER_2_BRIGHTNESS_VALUE__AnalogSerialOutput__ = 6;
    const uint COMPUTER_2_BRIGHTNESS_LEVEL__AnalogSerialOutput__ = 7;
    const uint DVI_CONTRAST_VALUE__AnalogSerialOutput__ = 8;
    const uint DVI_CONTRAST_LEVEL__AnalogSerialOutput__ = 9;
    const uint DVI_BRIGHTNESS_VALUE__AnalogSerialOutput__ = 10;
    const uint DVI_BRIGHTNESS_LEVEL__AnalogSerialOutput__ = 11;
    const uint VIDEO_CONTRAST_VALUE__AnalogSerialOutput__ = 12;
    const uint VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__ = 13;
    const uint VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__ = 14;
    const uint VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__ = 15;
    const uint S_VIDEO_CONTRAST_VALUE__AnalogSerialOutput__ = 16;
    const uint S_VIDEO_CONTRAST_LEVEL__AnalogSerialOutput__ = 17;
    const uint S_VIDEO_BRIGHTNESS_VALUE__AnalogSerialOutput__ = 18;
    const uint S_VIDEO_BRIGHTNESS_LEVEL__AnalogSerialOutput__ = 19;
    const uint LAMP_HOURS__AnalogSerialOutput__ = 20;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
