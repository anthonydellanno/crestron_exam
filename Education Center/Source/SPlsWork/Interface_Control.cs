using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_INTERFACE_CONTROL
{
    public class UserModuleClass_INTERFACE_CONTROL : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput PARTITION1ACTIVE;
        Crestron.Logos.SplusObjects.DigitalInput PARTITION1NOTACTIVE;
        Crestron.Logos.SplusObjects.DigitalInput PARTITION2ACTIVE;
        Crestron.Logos.SplusObjects.DigitalInput PARTITION2NOTACTIVE;
        Crestron.Logos.SplusObjects.DigitalInput M_VISION_PROJECTOR_IS_ONLINE;
        Crestron.Logos.SplusObjects.DigitalInput SHARP_PROJECTOR_IS_ONLINE;
        Crestron.Logos.SplusObjects.DigitalOutput SHARP_PROJECTOR_ON;
        Crestron.Logos.SplusObjects.DigitalOutput M_VISION_PROJECTOR_ON;
        Crestron.Logos.SplusObjects.DigitalOutput BTNMVISIONPROJECTORONVISIBILITY;
        Crestron.Logos.SplusObjects.DigitalOutput BTNMVISIONPROJECTOROFFVISIBILITY;
        Crestron.Logos.SplusObjects.DigitalOutput BTNSHARPPROJECTORONVISIBILITY;
        Crestron.Logos.SplusObjects.DigitalOutput BTNSHARPPROJECTOROFFVISIBILITY;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMASOURCE1BTN;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMASOURCE2BTN;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMBSOURCE1BTN;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMBSOURCE2BTN;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMCSOURCE1BTN;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMCSOURCE2BTN;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMA_ID;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMB_ID;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMC_ID;
        Crestron.Logos.SplusObjects.StringOutput ROOMA_ROOM_CONFIG;
        Crestron.Logos.SplusObjects.StringOutput ROOMB_ROOM_CONFIG;
        Crestron.Logos.SplusObjects.StringOutput ROOMC_ROOM_CONFIG;
        private void CONFIGUREPANEL (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 161;
            ROOMASOURCE1BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 162;
            ROOMASOURCE2BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 163;
            ROOMBSOURCE1BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 164;
            ROOMBSOURCE2BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 165;
            ROOMCSOURCE1BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 166;
            ROOMCSOURCE2BTN  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 168;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ROOMA_ID  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 170;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 171;
                    ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 172;
                    ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 174;
                    ROOMA_ROOM_CONFIG  .UpdateValue ( "B + C"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 176;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 177;
                        ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 178;
                        ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 180;
                        ROOMA_ROOM_CONFIG  .UpdateValue ( "All seperate"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 181;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 183;
                            ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 184;
                            ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 185;
                            ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 186;
                            ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 188;
                            ROOMA_ROOM_CONFIG  .UpdateValue ( "A + B"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 189;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 190;
                                ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 191;
                                ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 192;
                                ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 193;
                                ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 194;
                                ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 195;
                                ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 197;
                                ROOMA_ROOM_CONFIG  .UpdateValue ( "All combined"  ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 201;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ROOMB_ID  .Value == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 203;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 204;
                        ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 205;
                        ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 206;
                        ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 207;
                        ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 209;
                        ROOMB_ROOM_CONFIG  .UpdateValue ( "B + C"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 210;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 211;
                            ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 212;
                            ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 214;
                            ROOMB_ROOM_CONFIG  .UpdateValue ( "All seperate"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 215;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 216;
                                ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 217;
                                ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 218;
                                ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 219;
                                ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 221;
                                ROOMB_ROOM_CONFIG  .UpdateValue ( "A + B"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 222;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 223;
                                    ROOMASOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 224;
                                    ROOMASOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 225;
                                    ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 226;
                                    ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 227;
                                    ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 228;
                                    ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 230;
                                    ROOMB_ROOM_CONFIG  .UpdateValue ( "All combined"  ) ; 
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 234;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ROOMC_ID  .Value == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 236;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 237;
                            ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 238;
                            ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 239;
                            ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 240;
                            ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 242;
                            ROOMC_ROOM_CONFIG  .UpdateValue ( "B + C"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 243;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 1) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 244;
                                ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 245;
                                ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 247;
                                ROOMC_ROOM_CONFIG  .UpdateValue ( "All seperate"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 248;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 1) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 249;
                                    ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 250;
                                    ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 252;
                                    ROOMC_ROOM_CONFIG  .UpdateValue ( "A + B"  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 253;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PARTITION1ACTIVE  .Value == 0) ) && Functions.TestForTrue ( Functions.BoolToInt (PARTITION2ACTIVE  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 254;
                                        ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 255;
                                        ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 256;
                                        ROOMBSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 257;
                                        ROOMBSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 258;
                                        ROOMCSOURCE1BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 259;
                                        ROOMCSOURCE2BTN  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 261;
                                        ROOMC_ROOM_CONFIG  .UpdateValue ( "All combined"  ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 267;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (M_VISION_PROJECTOR_IS_ONLINE  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 268;
                BTNMVISIONPROJECTORONVISIBILITY  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 269;
                BTNMVISIONPROJECTOROFFVISIBILITY  .Value = (ushort) ( 1 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 272;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SHARP_PROJECTOR_IS_ONLINE  .Value == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 273;
                    BTNSHARPPROJECTORONVISIBILITY  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 274;
                    BTNSHARPPROJECTOROFFVISIBILITY  .Value = (ushort) ( 1 ) ; 
                    } 
                
                }
            
            
            }
            
        private void POLLPROJECTORS (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 281;
            Functions.Pulse ( 10000, SHARP_PROJECTOR_ON ) ; 
            __context__.SourceCodeLine = 282;
            Functions.Pulse ( 10000, M_VISION_PROJECTOR_ON ) ; 
            
            }
            
        object PARTITION1ACTIVE_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 313;
                Trace( "Partition 1 Active") ; 
                __context__.SourceCodeLine = 314;
                CONFIGUREPANEL (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object PARTITION2ACTIVE_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 319;
            Trace( "Partition 2 Active") ; 
            __context__.SourceCodeLine = 320;
            CONFIGUREPANEL (  __context__  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object PARTITION1NOTACTIVE_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 325;
        CONFIGUREPANEL (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PARTITION2NOTACTIVE_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 330;
        CONFIGUREPANEL (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object M_VISION_PROJECTOR_IS_ONLINE_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 335;
        CONFIGUREPANEL (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SHARP_PROJECTOR_IS_ONLINE_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 340;
        CONFIGUREPANEL (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 400;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 403;
        CONFIGUREPANEL (  __context__  ) ; 
        __context__.SourceCodeLine = 406;
        POLLPROJECTORS (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    
    PARTITION1ACTIVE = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION1ACTIVE__DigitalInput__, this );
    m_DigitalInputList.Add( PARTITION1ACTIVE__DigitalInput__, PARTITION1ACTIVE );
    
    PARTITION1NOTACTIVE = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION1NOTACTIVE__DigitalInput__, this );
    m_DigitalInputList.Add( PARTITION1NOTACTIVE__DigitalInput__, PARTITION1NOTACTIVE );
    
    PARTITION2ACTIVE = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION2ACTIVE__DigitalInput__, this );
    m_DigitalInputList.Add( PARTITION2ACTIVE__DigitalInput__, PARTITION2ACTIVE );
    
    PARTITION2NOTACTIVE = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION2NOTACTIVE__DigitalInput__, this );
    m_DigitalInputList.Add( PARTITION2NOTACTIVE__DigitalInput__, PARTITION2NOTACTIVE );
    
    M_VISION_PROJECTOR_IS_ONLINE = new Crestron.Logos.SplusObjects.DigitalInput( M_VISION_PROJECTOR_IS_ONLINE__DigitalInput__, this );
    m_DigitalInputList.Add( M_VISION_PROJECTOR_IS_ONLINE__DigitalInput__, M_VISION_PROJECTOR_IS_ONLINE );
    
    SHARP_PROJECTOR_IS_ONLINE = new Crestron.Logos.SplusObjects.DigitalInput( SHARP_PROJECTOR_IS_ONLINE__DigitalInput__, this );
    m_DigitalInputList.Add( SHARP_PROJECTOR_IS_ONLINE__DigitalInput__, SHARP_PROJECTOR_IS_ONLINE );
    
    SHARP_PROJECTOR_ON = new Crestron.Logos.SplusObjects.DigitalOutput( SHARP_PROJECTOR_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( SHARP_PROJECTOR_ON__DigitalOutput__, SHARP_PROJECTOR_ON );
    
    M_VISION_PROJECTOR_ON = new Crestron.Logos.SplusObjects.DigitalOutput( M_VISION_PROJECTOR_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( M_VISION_PROJECTOR_ON__DigitalOutput__, M_VISION_PROJECTOR_ON );
    
    BTNMVISIONPROJECTORONVISIBILITY = new Crestron.Logos.SplusObjects.DigitalOutput( BTNMVISIONPROJECTORONVISIBILITY__DigitalOutput__, this );
    m_DigitalOutputList.Add( BTNMVISIONPROJECTORONVISIBILITY__DigitalOutput__, BTNMVISIONPROJECTORONVISIBILITY );
    
    BTNMVISIONPROJECTOROFFVISIBILITY = new Crestron.Logos.SplusObjects.DigitalOutput( BTNMVISIONPROJECTOROFFVISIBILITY__DigitalOutput__, this );
    m_DigitalOutputList.Add( BTNMVISIONPROJECTOROFFVISIBILITY__DigitalOutput__, BTNMVISIONPROJECTOROFFVISIBILITY );
    
    BTNSHARPPROJECTORONVISIBILITY = new Crestron.Logos.SplusObjects.DigitalOutput( BTNSHARPPROJECTORONVISIBILITY__DigitalOutput__, this );
    m_DigitalOutputList.Add( BTNSHARPPROJECTORONVISIBILITY__DigitalOutput__, BTNSHARPPROJECTORONVISIBILITY );
    
    BTNSHARPPROJECTOROFFVISIBILITY = new Crestron.Logos.SplusObjects.DigitalOutput( BTNSHARPPROJECTOROFFVISIBILITY__DigitalOutput__, this );
    m_DigitalOutputList.Add( BTNSHARPPROJECTOROFFVISIBILITY__DigitalOutput__, BTNSHARPPROJECTOROFFVISIBILITY );
    
    ROOMASOURCE1BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMASOURCE1BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMASOURCE1BTN__DigitalOutput__, ROOMASOURCE1BTN );
    
    ROOMASOURCE2BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMASOURCE2BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMASOURCE2BTN__DigitalOutput__, ROOMASOURCE2BTN );
    
    ROOMBSOURCE1BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMBSOURCE1BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMBSOURCE1BTN__DigitalOutput__, ROOMBSOURCE1BTN );
    
    ROOMBSOURCE2BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMBSOURCE2BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMBSOURCE2BTN__DigitalOutput__, ROOMBSOURCE2BTN );
    
    ROOMCSOURCE1BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMCSOURCE1BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMCSOURCE1BTN__DigitalOutput__, ROOMCSOURCE1BTN );
    
    ROOMCSOURCE2BTN = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMCSOURCE2BTN__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMCSOURCE2BTN__DigitalOutput__, ROOMCSOURCE2BTN );
    
    ROOMA_ID = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMA_ID__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMA_ID__AnalogSerialOutput__, ROOMA_ID );
    
    ROOMB_ID = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMB_ID__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMB_ID__AnalogSerialOutput__, ROOMB_ID );
    
    ROOMC_ID = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMC_ID__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMC_ID__AnalogSerialOutput__, ROOMC_ID );
    
    ROOMA_ROOM_CONFIG = new Crestron.Logos.SplusObjects.StringOutput( ROOMA_ROOM_CONFIG__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMA_ROOM_CONFIG__AnalogSerialOutput__, ROOMA_ROOM_CONFIG );
    
    ROOMB_ROOM_CONFIG = new Crestron.Logos.SplusObjects.StringOutput( ROOMB_ROOM_CONFIG__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMB_ROOM_CONFIG__AnalogSerialOutput__, ROOMB_ROOM_CONFIG );
    
    ROOMC_ROOM_CONFIG = new Crestron.Logos.SplusObjects.StringOutput( ROOMC_ROOM_CONFIG__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMC_ROOM_CONFIG__AnalogSerialOutput__, ROOMC_ROOM_CONFIG );
    
    
    PARTITION1ACTIVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION1ACTIVE_OnPush_0, false ) );
    PARTITION2ACTIVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION2ACTIVE_OnPush_1, false ) );
    PARTITION1NOTACTIVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION1NOTACTIVE_OnPush_2, false ) );
    PARTITION2NOTACTIVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION2NOTACTIVE_OnPush_3, false ) );
    M_VISION_PROJECTOR_IS_ONLINE.OnDigitalPush.Add( new InputChangeHandlerWrapper( M_VISION_PROJECTOR_IS_ONLINE_OnPush_4, false ) );
    SHARP_PROJECTOR_IS_ONLINE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SHARP_PROJECTOR_IS_ONLINE_OnPush_5, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_INTERFACE_CONTROL ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint PARTITION1ACTIVE__DigitalInput__ = 0;
const uint PARTITION1NOTACTIVE__DigitalInput__ = 1;
const uint PARTITION2ACTIVE__DigitalInput__ = 2;
const uint PARTITION2NOTACTIVE__DigitalInput__ = 3;
const uint M_VISION_PROJECTOR_IS_ONLINE__DigitalInput__ = 4;
const uint SHARP_PROJECTOR_IS_ONLINE__DigitalInput__ = 5;
const uint SHARP_PROJECTOR_ON__DigitalOutput__ = 0;
const uint M_VISION_PROJECTOR_ON__DigitalOutput__ = 1;
const uint BTNMVISIONPROJECTORONVISIBILITY__DigitalOutput__ = 2;
const uint BTNMVISIONPROJECTOROFFVISIBILITY__DigitalOutput__ = 3;
const uint BTNSHARPPROJECTORONVISIBILITY__DigitalOutput__ = 4;
const uint BTNSHARPPROJECTOROFFVISIBILITY__DigitalOutput__ = 5;
const uint ROOMASOURCE1BTN__DigitalOutput__ = 6;
const uint ROOMASOURCE2BTN__DigitalOutput__ = 7;
const uint ROOMBSOURCE1BTN__DigitalOutput__ = 8;
const uint ROOMBSOURCE2BTN__DigitalOutput__ = 9;
const uint ROOMCSOURCE1BTN__DigitalOutput__ = 10;
const uint ROOMCSOURCE2BTN__DigitalOutput__ = 11;
const uint ROOMA_ID__AnalogSerialOutput__ = 0;
const uint ROOMB_ID__AnalogSerialOutput__ = 1;
const uint ROOMC_ID__AnalogSerialOutput__ = 2;
const uint ROOMA_ROOM_CONFIG__AnalogSerialOutput__ = 3;
const uint ROOMB_ROOM_CONFIG__AnalogSerialOutput__ = 4;
const uint ROOMC_ROOM_CONFIG__AnalogSerialOutput__ = 5;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
